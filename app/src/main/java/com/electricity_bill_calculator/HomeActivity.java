package com.electricity_bill_calculator;

import android.app.Activity;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.electricity_bill_calculator.Adapter.Adapter_Home_ViewPager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.viewpager) ViewPager viewpager;
    @BindView(R.id.btnTab1) Button btnTab1;
    @BindView(R.id.btnTab2) Button btnTab2;
//    @BindView(R.id.btnTab3) Button btnTab3;
    @BindView(R.id.btnTab4) Button btnTab4;
    @BindView(R.id.btnTab5) Button btnTab5;

    public Activity activity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setUI();
        setInit();
    }

    private void setUI() {
        Adapter_Home_ViewPager adapterMainViewPager = new Adapter_Home_ViewPager(getSupportFragmentManager());
        viewpager.setAdapter(adapterMainViewPager);
    }

    private void setInit() {
        activity = HomeActivity.this;

        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                switch (position) {
                    case 0: {
                        setTabs(1);
                        break;
                    }
                    case 1: {
                        setTabs(5);
                        break;
                    }
                    case 2: {
                        setTabs(2);
                        break;
                    }
//                    case 3: {
//                        setTabs(3);
//                        break;
//                    }
                    case 3: {
                        setTabs(4);
                        break;
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setEvent() {

    }

    @OnClick({R.id.btnTab1, R.id.btnTab2, R.id.btnTab4, R.id.btnTab5})
    void setBtn(View view) {
        switch (view.getId()) {
            case R.id.btnTab1:
                viewpager.setCurrentItem(0);
                break;
            case R.id.btnTab5:
                viewpager.setCurrentItem(1);
                break;
            case R.id.btnTab2:
                viewpager.setCurrentItem(2);
                break;
//            case R.id.btnTab3:
//                viewpager.setCurrentItem(3);
//                break;
            case R.id.btnTab4:
                viewpager.setCurrentItem(3);
                break;

        }
    }

    private void setTabs(int i){
        btnTab1.setBackgroundResource(R.drawable.home_tab_01_off);
        btnTab5.setBackgroundResource(R.drawable.home_tab_05_off);
        btnTab2.setBackgroundResource(R.drawable.home_tab_02_off);
//        btnTab3.setBackgroundResource(R.drawable.home_tab_03_off);
        btnTab4.setBackgroundResource(R.drawable.home_tab_04_off);


        if(i == 1){
            btnTab1.setBackgroundResource(R.drawable.home_tab_01_on);
        }else if(i == 2){
            btnTab2.setBackgroundResource(R.drawable.home_tab_02_on);
        }else if(i == 3){
//            btnTab3.setBackgroundResource(R.drawable.home_tab_03_on);
        }else if(i == 4){
            btnTab4.setBackgroundResource(R.drawable.home_tab_04_on);
        }else if(i == 5) {
            btnTab5.setBackgroundResource(R.drawable.home_tab_05_on);
        }
    }
}
