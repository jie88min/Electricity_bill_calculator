package com.electricity_bill_calculator.DTO;

/**
 * Created by 김지민 on 2017-07-30.
 */

public class MyElectricity {
    private String DATE;    // 날짜
    private String ELECT;   // 전력량
    private String CHANGE;  // 요금

    public MyElectricity(String DATE, String ELECT, String CHANGE) {
        this.DATE = DATE;
        this.ELECT = ELECT;
        this.CHANGE = CHANGE;
    }

    public String getDATE() {
        return DATE;
    }

    public void setDATE(String DATE) {
        this.DATE = DATE;
    }

    public String getELECT() {
        return ELECT;
    }

    public void setELECT(String ELECT) {
        this.ELECT = ELECT;
    }

    public String getCHANGE() {
        return CHANGE;
    }

    public void setCHANGE(String CHANGE) {
        this.CHANGE = CHANGE;
    }
}
