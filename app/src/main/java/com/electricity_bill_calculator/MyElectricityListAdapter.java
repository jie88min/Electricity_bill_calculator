package com.electricity_bill_calculator;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.electricity_bill_calculator.DTO.MyElectricity;

import java.util.List;

/**
 * Created by 김지민 on 2017-07-09.
 */

public class MyElectricityListAdapter extends BaseAdapter {
    private Context context;
    private List<com.electricity_bill_calculator.DTO.MyElectricity> MyElectricity;
    private LayoutInflater mInflater;
    private int mLayout;
    private setElect setElect;

    public MyElectricityListAdapter(Context context, int layout, List<MyElectricity> MyElectricity) {
        this.context = context;
        this.mLayout = layout;
        this.MyElectricity = MyElectricity;
        this.mInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        setElect = new setElect();
    }

    @Override
    public int getCount() {
        return MyElectricity.size();
    }

    @Override
    public Object getItem(int position) {
        return MyElectricity.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup viewGroup) {
        final Context context = mInflater.getContext();
        final MyElectricity myElectricity = MyElectricity.get(position);


        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.elect_list, viewGroup, false);
        }
        setElect.electlist_date = (TextView) convertView.findViewById(R.id.electlist_date);
        setElect.electlist_elect = (TextView) convertView.findViewById(R.id.electlist_elect);
        setElect.electlist_change = (TextView) convertView.findViewById(R.id.electlist_change);

        setElect.electlist_date.setText(myElectricity.getDATE());

        setElect.electlist_elect.setText(String.format("%,d", Math.round(Float.parseFloat(myElectricity.getELECT()))) + " kWh");

        setElect.electlist_change.setText(setChange(myElectricity.getCHANGE()));
        setChangeColor(setElect.electlist_change, myElectricity.getCHANGE());


        return convertView;
    }


    public class setElect{
        public TextView electlist_date;
        public TextView electlist_elect;
        public TextView electlist_change;
    }

    private String setChange(String elect){
        float ielect = Float.parseFloat(elect);
        if (ielect == 0) {
            return "";
        }else if (ielect > 0){
            return "(+" + String.format("%,d", Math.round(ielect)) + ")";
        } else {
            return "(" + String.format("%,d", Math.round(ielect)) + ")";
        }
    }

    private void setChangeColor(TextView textView, String elect){
        float ielect = Float.parseFloat(elect);
        if (ielect == 0) {
            textView.setTextColor(context.getResources().getColor(R.color.toolbar_text));
        } else if (ielect > 0) {
            textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            textView.setTextColor(context.getResources().getColor(R.color.colorAccent));
        }
    }
}

