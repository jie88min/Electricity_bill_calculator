package com.electricity_bill_calculator;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuPopupHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.electricity_bill_calculator.Utils.CircularProgressBar;
import com.electricity_bill_calculator.Utils.CustomProgressDialog;
import com.electricity_bill_calculator.Utils.Electricity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.tsengvn.typekit.TypekitContextWrapper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import android.content.pm.PackageManager.NameNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import me.grantland.widget.AutofitTextView;

public class AnalysisActivity extends AppCompatActivity implements View.OnClickListener{
    public static int Requisition_amount=0, Welfare=0, LargeFamily=0, Electric_Charge=0;
    private String Total_electricity="", Before_Month_electricity="", Electricity_Pressure="", Last_date="";
    public static String ELECT = "";
    private int mMax_charge=0, sp_max=0;
    public int Base_Charge = 0;
    public int Total_Charge = 0;
    public int persent=0;

    private boolean mSuper_user = false;
    private boolean toggle = false;
    private boolean progressive = false;

    public SharedPreferences settings;

    private TextView Use_charge, detail;
    private AutofitTextView txt0, txt1, txt2, date, progressive_tax;
    private TextView percent_num;
    private ImageView Siren, menu_badge, branch, menu_button;

    private LinearLayout button_detail, button_siren, button_use;

    private CircularProgressBar circularProgressBar1;

    private LinearLayout progressive_view;

    private long lastTimeBackPressed;

    private String storeVersion = "", deviceVersion = "";
    private int NeedUpdate = 0;
    private View mDialog;
    private Electricity electricity;
    private FloatingActionButton Quick_Add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_analysis);

        setItem();

        new Version().execute();
        setAd();

        setQuick_Add();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    // 객체 연결
    private void setItem() {
        date = (AutofitTextView) findViewById(R.id.date);
        Use_charge = (TextView) findViewById(R.id.Use_charge);
        detail = (TextView) findViewById(R.id.detail);
        menu_badge = (ImageView) findViewById(R.id.menu_badge);
        percent_num = (TextView) findViewById(R.id.percent);

        txt0 = (AutofitTextView) findViewById(R.id.txt0);
        txt1 = (AutofitTextView) findViewById(R.id.txt1);
        txt2 = (AutofitTextView) findViewById(R.id.txt2);

        button_detail = (LinearLayout) findViewById(R.id.button_detail);
        button_detail.setOnClickListener(this);

        button_siren = (LinearLayout) findViewById(R.id.button_siren);
        button_siren.setOnClickListener(this);

        button_use = (LinearLayout) findViewById(R.id.button_use);
        button_use.setOnClickListener(this);

        menu_button = (ImageView) findViewById(R.id.menu_button);
        menu_button.setOnClickListener(this);

        circularProgressBar1 = (CircularProgressBar) findViewById(R.id.circularProgress);
        circularProgressBar1.setOnClickListener(this);
        Quick_Add = (FloatingActionButton) findViewById(R.id.Quick_Add);

        progressive_tax = (AutofitTextView) findViewById(R.id.progressive_tax);
        branch = (ImageView) findViewById(R.id.branch);
        progressive_view = (LinearLayout) findViewById(R.id.progressive_view);

        electricity = new Electricity();
        setData();
    }

    // 현재 날짜 반환
    public String getDateString(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        String str_date = df.format(new Date());

        return str_date;
    }

    // main Activity 이동
    private void intent_main(String menu_item){
        Intent intent = new Intent(this, MainActivity.class);
        //intent.setFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        if (menu_item.equals("modify")){
            intent.putExtra("menu_item", "1");
            intent.putExtra("Total_electricity", Total_electricity);
            intent.putExtra("Before_Month_electricity", Before_Month_electricity);
            intent.putExtra("Electricity_Pressure", Electricity_Pressure);
            intent.putExtra("Welfare", String.valueOf(Welfare));
            intent.putExtra("LargeFamily", String.valueOf(LargeFamily));
            intent.putExtra("mSuper_user", String.valueOf(mSuper_user));
        }else{
            intent.putExtra("menu_item", "0");
        }
        startActivityForResult(intent, 0);
    }

    // 데이터 세팅
    private void setData(){
        getData();
        setText_Amount();
        date.setText("마지막 계산일 : " + Last_date);
        if (!getDateString().equals(Last_date)){
            date.setText("사용량을 수정해 주세요.");
        }
        Use_charge.setText(Integer.parseInt(Total_electricity) - Integer.parseInt(Before_Month_electricity) + "");
        setSiren();
        setPercent();
        setProgress();
        setProgressive_tax();

    }

    // 데이터 읽기
    private void getData(){
        settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);

        Total_electricity = settings.getString("Total_electricity", "0");
        Before_Month_electricity = settings.getString("Before_Month_electricity", "0");
        Electricity_Pressure = settings.getString("Electricity_Pressure", "LOW");
        mMax_charge = Integer.parseInt(settings.getString("mMax_charge", "100000"));
        Last_date = settings.getString("Last_date", getDateString());
        Welfare = settings.getInt("Welfare", 0);
        LargeFamily = settings.getInt("LargeFamily", 0);
        Requisition_amount = settings.getInt("Requisition_amount", 0);
        Base_Charge = settings.getInt("Base_Charge", 0);
        Total_Charge = settings.getInt("Total_Charge", 0);
        Electric_Charge = settings.getInt("Electric_Charge", 0);
        mSuper_user = settings.getBoolean("mSuper_user", false);
        ELECT = settings.getString("ELECT", "");
    }

    // 누진세 알림 세팅
    private void setProgressive_tax(){
        electricity.progressive_tax(Integer.parseInt(Total_electricity) - Integer.parseInt(Before_Month_electricity));

        progressive_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!progressive){
                    date.setText(electricity.progressive_tax(Integer.parseInt(Total_electricity) - Integer.parseInt(Before_Month_electricity)));
                    progressive = true;
                } else {
                    date.setText("마지막 계산일 : " + Last_date);
                    if (!getDateString().equals(Last_date)){
                        date.setText("사용량을 수정해 주세요.");
                    }
                    progressive = false;
                }
            }
        });

        if (electricity.progressivetax == 0){
            progressive_tax.setVisibility(View.GONE);
            branch.setVisibility(View.VISIBLE);
        } else if (electricity.progressivetax == 1){
            progressive_tax.setVisibility(View.VISIBLE);
            branch.setVisibility(View.GONE);
            progressive_tax.setText("1단계");
        } else if (electricity.progressivetax == 2) {
            progressive_tax.setVisibility(View.VISIBLE);
            branch.setVisibility(View.GONE);
            progressive_tax.setText("2단계");
            progressive_tax.setTextColor(getResources().getColor(R.color.colorYellow));
        }


    }

    // 사이렌 세팅
    private void setSiren(){
        persent = (int)(((double)Requisition_amount/(double)mMax_charge)*100);
        Siren = (ImageView) findViewById(R.id.Siren);
        if (persent < 40) {
            Siren.setImageDrawable(getResources().getDrawable(R.drawable.alarm_green));
        }else if (persent >= 40 && persent < 80){
            Siren.setImageDrawable(getResources().getDrawable(R.drawable.alarm_warning));
        }else{
            Siren.setImageDrawable(getResources().getDrawable(R.drawable.alarm_red));
        }
    }
    private int getSiren(){
        //getData();
        persent = (int)(((double)Requisition_amount/(double)mMax_charge)*100);
        int siren;
        if (persent < 40) {
            siren = 0;
        }else if (persent >= 40 && persent < 80){
            siren = 1;
        }else{
            siren = 2;
        }
        return siren;
    }
    private void dialog_siren(){
        LayoutInflater inflater = getLayoutInflater();
        mDialog = inflater.inflate(R.layout.dialog_siren, null);
        setDialog_Siren setDialog_Siren = new setDialog_Siren();
        AlertDialog.Builder buider= new AlertDialog.Builder(this); //AlertDialog.Builder 객체 생성
        buider.setView(mDialog);

        setDialog_Siren.img_1 = (ImageView) mDialog.findViewById(R.id.img_1);
        setDialog_Siren.txt_help = (TextView) mDialog.findViewById(R.id.txt_help);
        setDialog_Siren.txt_tip = (TextView) mDialog.findViewById(R.id.txt_tip);
        setDialog_Siren.btn_done = (Button) mDialog.findViewById(R.id.btn_done);

        setDialog_Siren.txt_tip.setText(getTip());

        switch (getSiren()) {
            case 0:
                setDialog_Siren.img_1.setImageDrawable(getResources().getDrawable(R.drawable.alarm_green));
                setDialog_Siren.txt_help.setText("좋아요! 전기 절약!");
                break;
            case 1:
                setDialog_Siren.img_1.setImageDrawable(getResources().getDrawable(R.drawable.alarm_warning));
                setDialog_Siren.txt_help.setText("멋져요! 좀 더 힘내요!");
                break;
            case 2:
                setDialog_Siren.img_1.setImageDrawable(getResources().getDrawable(R.drawable.alarm_red));
                setDialog_Siren.txt_help.setText("위험! 전기 절약에 힘써야 해요!");
                break;
            default:
                break;
        }

        final AlertDialog dialog = buider.create();
        setDialog_Siren.btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);//없어지도록 설정
        dialog.show();
    }
    private String getTip(){
        String result = "";
        int random = (int) (Math.random() * 8);
        if (random == 0){
            result += "사용량을 자주 수정해 줄 수록 정확한 요금을 알 수 있습니다.";
        }else if (random == 1){
            result += "냉장고 문을 잠깐만 열었다 닫아도 전력 소모가 커 문을 자주 열지만 않아도 그만큼 전기를 아낄 수 있죠.";
        }else if (random == 2){
            result += "냉장고의 적정 온도를 맞추는 것도 중요합니다.\n특히 보관하는 내용물이 많을수록 온도유지를 위해 전력이 낭비될 수도 있으니 주의하세요!";
        }else if (random == 3){
            result += "집집마다 놓여있는 TV의 전력소모량도 무시할 수 없습니다.\n자체 스위치를 꺼도 플러그가 꼽혀 있다면 일정량의 대기전력이 소모되기 때문이죠.";
        }else if (random == 4){
            result += "책상 위에 놓여있는 컴퓨터 역시 불필요하게 소모되는 전력이 만만치 않습니다.\n항상 꽂아둔 USB와 화려한 스크린세이버는 전력소비에 탁월하죠.";
        }else if (random == 5){
            result += "에어컨의 경우 처음 가동할 때 가장 많은 전기가 소모되므로 일단 강하게 튼 뒤 점차 온도를 맞추는 것이 좋습니다.";
        }else if (random == 6){
            result += "전기밥솥의 보온기능은 전기요금 상승의 주범!";
        }else if (random == 7){
            result += "청소기는 필터의 먼지만 자주 제거해 주셔도 전력 소모를 줄이는데 큰 효과를 보실 수 있습니다.";
        }

        return result;
    }

    // 한도 세팅
    private void dialog_max(){
        LayoutInflater inflater = getLayoutInflater();
        mDialog = inflater.inflate(R.layout.dialog_max, null);
        setDialog_Max setDialog_Max = new setDialog_Max();
        AlertDialog.Builder buider= new AlertDialog.Builder(this); //AlertDialog.Builder 객체 생성
        buider.setView(mDialog);

        setDialog_Max.txt_dialog_title = (TextView) mDialog.findViewById(R.id.txt_dialog_title);
        setDialog_Max.Max_Spinner = (Spinner) mDialog.findViewById(R.id.Max_Spinner);
        Max_list_adapter(setDialog_Max.Max_Spinner, 0);
        setDialog_Max.txt_help = (TextView) mDialog.findViewById(R.id.txt_help);
        setDialog_Max.btn_done = (Button) mDialog.findViewById(R.id.btn_done);
        setDialog_Max.btn_cancel = (Button) mDialog.findViewById(R.id.btn_cancel);

        setDialog_Max.txt_dialog_title.setText("한도 설정");
        setDialog_Max.txt_help.setText("최대 한도를 설정합니다.");

        final AlertDialog dialog = buider.create();

        setDialog_Max.btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();

                editor.putString("mMax_charge", String.valueOf(Max_value()));
                editor.apply();

                setData();
                setProgress();
                dialog.dismiss();
            }
        });

        setDialog_Max.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }
    private void Max_list_adapter(Spinner Max_Spinner, int item){
        final String[] Welfare_list = getResources().getStringArray(R.array.Max);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Welfare_list);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Max_Spinner.setAdapter(adapter);
        Max_Spinner.setSelection(item);

        Max_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sp_max = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
    private int Max_value(){
        int result = 10000;
        if (sp_max==0){
            result=10000;
        }else if (sp_max<=9 && sp_max>0){
            result=(sp_max+1)*10000;
        }else if (sp_max==10){
            result=(sp_max+5)*10000;
        }else if (sp_max==11){
            result=(sp_max+9)*10000;
        }else if (sp_max==12){
            result=(sp_max+13)*10000;
        }else if (sp_max==13){
            result=(sp_max+17)*10000;
        }
        return result;
    }

    // 계산 내역 세팅
    private void dialog_detail(){
        LayoutInflater inflater = getLayoutInflater();
        mDialog = inflater.inflate(R.layout.dialog_detail, null);
        setDialog_Detail setDialog_Detail = new setDialog_Detail();
        AlertDialog.Builder buider= new AlertDialog.Builder(this); //AlertDialog.Builder 객체 생성
        buider.setView(mDialog);

        setDialog_Detail.txt_dialog_title = (TextView) mDialog.findViewById(R.id.txt_dialog_title);
        setDialog_Detail.txt_help = (TextView) mDialog.findViewById(R.id.txt_help);
        setDialog_Detail.detail_Base_Charge = (TextView) mDialog.findViewById(R.id.detail_Base_Charge);
        setDialog_Detail.detail_Electric_Charge = (TextView) mDialog.findViewById(R.id.detail_Electric_Charge);
        setDialog_Detail.detail_Total_Charge = (TextView) mDialog.findViewById(R.id.detail_Total_Charge);
        setDialog_Detail.detail_Total_Charge_add = (TextView) mDialog.findViewById(R.id.detail_Total_Charge_add);
        setDialog_Detail.detail_Total_Charge_donate = (TextView) mDialog.findViewById(R.id.detail_Total_Charge_donate);
        setDialog_Detail.detail_Requisition_amount = (TextView) mDialog.findViewById(R.id.detail_Requisition_amount);
        setDialog_Detail.btn_done = (Button) mDialog.findViewById(R.id.btn_done);

        setDialog_Detail.txt_dialog_title.setText("계산 내역");
        setDialog_Detail.detail_Base_Charge.setText(String.format("%,d", Math.round(Base_Charge)) + "원");
        setDialog_Detail.detail_Electric_Charge.setText(String.format("%,d", Math.round(Electric_Charge)) + "원");
        setDialog_Detail.detail_Total_Charge.setText(String.format("%,d", Math.round(Total_Charge)) + "원");
        setDialog_Detail.detail_Total_Charge_add.setText(String.format("%,d", Math.round(Total_Charge * 0.1)) + "원");
        setDialog_Detail.detail_Total_Charge_donate.setText(String.format("%,d", Math.round(Total_Charge * 0.037)) + "원");
        setDialog_Detail.detail_Requisition_amount.setText(String.format("%,d", Math.round(Requisition_amount)) + "원");

        final AlertDialog dialog = buider.create();

        setDialog_Detail.btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    // 팝업메뉴 설정
    private void Popupmenu(View view){
        MenuBuilder menuBuilder =new MenuBuilder(this);
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.menu_analysis, menuBuilder);
        MenuPopupHelper optionsMenu = new MenuPopupHelper(this, menuBuilder, view);
        optionsMenu.setForceShowIcon(true);
        // Set Item Click Listener
        menuBuilder.setCallback(new MenuBuilder.Callback() {
            @Override
            public boolean onMenuItemSelected(MenuBuilder menu, MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.modify:
                        intent_main("modify");
                        return true;
                    case R.id.max:
                        dialog_max();
                        return true;
                    case R.id.useage:
                        Intent intent = new Intent(AnalysisActivity.this, UsagetableActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.version:
                        dialog_version();
                        return true;
                    case R.id.star:
                        Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                        marketLaunch.setData(Uri
                                .parse("https://play.google.com/store/apps/details?id=com.electricity_bill_calculator"));
                        startActivity(marketLaunch);
                        return true;
                    default:
                        return false;
                }
            }

            @Override
            public void onMenuModeChange(MenuBuilder menu) {}
        });
        optionsMenu.show();
    }

    // Admob 연결
    private void setAd(){
        MobileAds.initialize(getApplicationContext(), getString(R.string.banner_ad_unit_id));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    // 뱃지 설정
    private void setBadge(){
        if (NeedUpdate == 0){
            menu_badge.setVisibility(View.GONE);
        }else{
            menu_badge.setVisibility(View.VISIBLE);
        }
    }

    // 사용량 - %
    private void setPercent(){
        percent_num.setText(persent + "%");
    }

    // 사용량 - 그래프
    private void setProgress(){
        circularProgressBar1.setProgressColor(getResources().getColor(R.color.colorBrown2));
        circularProgressBar1.setTextColor(getResources().getColor(R.color.colorBrown2));
        circularProgressBar1.setProgressWidth(15);
        circularProgressBar1.setNum(Requisition_amount);
        circularProgressBar1.setmMax_charge(mMax_charge);
        circularProgressBar1.setProgress(persent);
    }

    // 예상요금 세팅
    private void setText_Amount(){
        String str = String.format("%,d", Math.round(Requisition_amount));
        txt0.setText("이번달 예상 요금은 ");
        txt1.setText(str);
        txt2.setText(" 원 입니다.");
    }

    // 사용량 세팅
    private void setText_Use(){
        String str = String.format("%,d", Math.round(Integer.parseInt(Total_electricity) - Integer.parseInt(Before_Month_electricity)));
        txt0.setText("이번달 전력 사용량은 ");
        txt1.setText(str);
        txt2.setText(" kWh 입니다.");
        circularProgressBar1.setNum(Integer.parseInt(Total_electricity) - Integer.parseInt(Before_Month_electricity));
    }

    @Override
    public void onBackPressed(){
        if(System.currentTimeMillis() - lastTimeBackPressed < 1500)
        {
            finish();
            return;
        }
        Toast.makeText(AnalysisActivity.this, "'뒤로'를 한 번 더 눌러 종료합니다.", Toast.LENGTH_SHORT).show();
        lastTimeBackPressed = System.currentTimeMillis();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu_button:
                Popupmenu(v);
                break;
            case R.id.circularProgress:
                Intent intent = new Intent(AnalysisActivity.this, UsagetableActivity.class);
                startActivity(intent);
                break;
            case R.id.button_siren:
                dialog_siren();
                break;
            case R.id.button_detail:
                dialog_detail();
                break;
            case R.id.button_use:
                if (!toggle){
                    setText_Use();
                    toggle = true;
                }else{
                    setProgress();
                    toggle = false;
                }
                break;
        }
    }

    // 버전 체크
    private class Version extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... params) {

            // Confirmation of market information in the Google Play Store
            try {
                Document doc = Jsoup
                        .connect(
                                "https://play.google.com/store/apps/details?id=com.electricity_bill_calculator")
                        .get();
                Elements Version = doc.select(".content");

                for (Element v : Version) {
                    if (v.attr("itemprop").equals("softwareVersion")) {
                        storeVersion = v.text();
                    }
                }
                return storeVersion;
            } catch (IOException e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            // Version check the execution application.
            PackageInfo pi = null;
            try {
                pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            } catch (NameNotFoundException e) {
                e.printStackTrace();
            }
            deviceVersion = pi.versionName;
            storeVersion = result;

            if (!deviceVersion.equals(storeVersion)) {
                // 업데이트 필요
                Log.d("VersionCheck","FAIL");
                NeedUpdate = 1;
                setBadge();
            }else{
                Log.d("VersionCheck","SUCCESS");
                //업데이트 불필요
            }

            super.onPostExecute(result);
        }
    }
    private void dialog_version(){
        LayoutInflater inflater = getLayoutInflater();
        mDialog = inflater.inflate(R.layout.dialog_help1, null);
        String str = "";
        setDialog_Version setDialog_Version = new setDialog_Version();
        AlertDialog.Builder buider= new AlertDialog.Builder(this); //AlertDialog.Builder 객체 생성
        buider.setView(mDialog);

        setDialog_Version.txt_dialog_title = (TextView) mDialog.findViewById(R.id.txt_dialog_title);
        setDialog_Version.txt_dialog_title.setText("버전 확인");
        setDialog_Version.img_1 = (ImageView) mDialog.findViewById(R.id.img_1);
        setDialog_Version.img_1.setVisibility(View.GONE);
        setDialog_Version.txt_help = (TextView) mDialog.findViewById(R.id.txt_help);
        setDialog_Version.txt_help.setTextSize(18);
        setDialog_Version.btn_done = (Button) mDialog.findViewById(R.id.btn_done);

        final AlertDialog dialog = buider.create();
        if (!storeVersion.equals("") && !deviceVersion.equals("")){
            if (storeVersion.equals(deviceVersion)){
                str = "\n\n최신 버전 입니다.\n";
            }else{
                str = "\n\n업데이트가 필요 합니다.\n";
                setDialog_Version.btn_done.setText("업데이트");
            }
            setDialog_Version.txt_help.setText("\n최신 버전 : " + storeVersion + "\n현재 버전 : " + deviceVersion + str);
        }else{
            setDialog_Version.txt_help.setText("\n네트워크 상태가 좋지 않습니다.\n연결 상태를 확인해 주세요.\n");
        }

        setDialog_Version.btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkConnection()){
                    if (storeVersion.equals(deviceVersion)){
                        dialog.dismiss();
                    }else{
                        NeedUpdate = 0;
                        Intent marketLaunch = new Intent(
                                Intent.ACTION_VIEW);
                        marketLaunch.setData(Uri
                                .parse("https://play.google.com/store/apps/details?id=com.electricity_bill_calculator"));
                        startActivity(marketLaunch);
                        dialog.dismiss();
                    }
                }else{
                    dialog.dismiss();
                }
            }
        });

        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (resultCode){
            case 1:
                setData();
                setProgress();
                Snackbar.make(findViewById(android.R.id.content), "사용량이 수정되었습니다.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                break;
            default:
                break;
        }
    }

    // 네트워크 체크
    private boolean NetworkConnection(){
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isMobileConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).isConnectedOrConnecting();
        boolean isWifiAvailable = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isAvailable();
        boolean isWifiConnect = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnectedOrConnecting();
        if((isWifiAvailable && isWifiConnect) || (isMobileConnect)) {
            return true;
        } else {
            return false;
        }
    }

    // 빠른 사용량 입력
    public void setQuick_Add(){
        Quick_Add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_Quick();
            }
        });
    }
    private void dialog_Quick(){
        LayoutInflater inflater = getLayoutInflater();
        mDialog = inflater.inflate(R.layout.dialog_quick, null);
        final setDialog_Quick setDialog_Quick = new setDialog_Quick();
        AlertDialog.Builder buider= new AlertDialog.Builder(this); //AlertDialog.Builder 객체 생성
        buider.setView(mDialog);

        setDialog_Quick.txt_dialog_title = (TextView) mDialog.findViewById(R.id.txt_dialog_title);
        setDialog_Quick.txt_help = (TextView) mDialog.findViewById(R.id.txt_help);

        setDialog_Quick.txt_total = (TextView) mDialog.findViewById(R.id.txt_total);
        setDialog_Quick.txt_total.setText(String.format("%,d", Math.round(Float.parseFloat(Total_electricity))) + " kWh");

        setDialog_Quick.txt_before = (TextView) mDialog.findViewById(R.id.txt_before);
        setDialog_Quick.txt_before.setText(String.format("%,d", Math.round(Float.parseFloat(Before_Month_electricity))) + " kWh");

        setDialog_Quick.Total_electricity = (EditText) mDialog.findViewById(R.id.Total_electricity) ;

        setDialog_Quick.btn_done = (Button) mDialog.findViewById(R.id.btn_done);

        setDialog_Quick.btn_cancel = (Button) mDialog.findViewById(R.id.btn_cancel);

        final AlertDialog dialog = buider.create();

        setDialog_Quick.btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                focusOff(setDialog_Quick.Total_electricity);
                if (setDialog_Quick.Total_electricity.getText().toString().trim().equals("")){
                    Snackbar.make(findViewById(android.R.id.content), "사용량을 입력하지 않았습니다.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (setDialog_Quick.Total_electricity.getText().toString().trim().equals("0")){
                    Snackbar.make(findViewById(android.R.id.content), "사용량에 0을 입력하실 수 없습니다.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (Integer.parseInt(setDialog_Quick.Total_electricity.getText().toString().trim()) < Integer.parseInt(Before_Month_electricity)) {
                    Snackbar.make(findViewById(android.R.id.content), "사용량이 전월지침보다 작을 수 없습니다.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                } else if (electricity.Electvalidate(ELECT) && !ELECT.equals("")){
                    Snackbar.make(findViewById(android.R.id.content), "사용량이 수정되었습니다.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("Last_date", getDateString());
                    editor.putString("Total_electricity", setDialog_Quick.Total_electricity.getText().toString());
                    editor.putString("ELECT", electricity.modifyUsageElect(ELECT, setDialog_Quick.Total_electricity.getText().toString()));
                    editor.putInt("Requisition_amount", getEstimated_charge(setDialog_Quick.Total_electricity.getText().toString()));
                    editor.apply();

                    setData();

                    dialog.dismiss();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "사용량이 입력되었습니다.", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();

                    settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString("Last_date", getDateString());
                    editor.putString("Total_electricity", setDialog_Quick.Total_electricity.getText().toString());
                    editor.putString("ELECT", electricity.addUsageElect(ELECT, setDialog_Quick.Total_electricity.getText().toString()));
                    editor.putInt("Requisition_amount", getEstimated_charge(setDialog_Quick.Total_electricity.getText().toString()));

                    editor.apply();

                    setData();

                    dialog.dismiss();
                }
                Log.d("ELECT", ELECT);
            }
        });

        setDialog_Quick.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    // 예상요금 계산
    public int getEstimated_charge(String Total_electricity){
        int ReqUse_Charge = 0;

        int Over_Charge = 0;
        int Count_200kwh = 200;
        int Count_400kwh = 400;
        int Count_1000kwh = 1000;
        int Discount = 0;

        int getTotal_electricity = Integer.parseInt(Total_electricity);
        int getBefore_Month_electricity = Integer.parseInt(Before_Month_electricity);
        int Use_electricity = getTotal_electricity - getBefore_Month_electricity;

        float L_CHARGE1 = Float.parseFloat(getString(R.string.RC_LOW_1));
        float L_CHARGE2 = Float.parseFloat(getString(R.string.RC_LOW_2));
        float L_CHARGE3 = Float.parseFloat(getString(R.string.RC_LOW_3));
        float L_CHARGE4 = Float.parseFloat(getString(R.string.RC_LOW_4));

        float H_CHARGE1 = Float.parseFloat(getString(R.string.RC_HIGH_1));
        float H_CHARGE2 = Float.parseFloat(getString(R.string.RC_HIGH_2));
        float H_CHARGE3 = Float.parseFloat(getString(R.string.RC_HIGH_3));
        float H_CHARGE4 = Float.parseFloat(getString(R.string.RC_HIGH_4));

        float Industry_fund = Float.parseFloat(getString(R.string.Industry_fund));
        float Added_tax = Float.parseFloat(getString(R.string.Added_tax));

        if (Electricity_Pressure.equals("LOW")){
            // 저압 요금 : 200kWh 이하 사용
            // 1단계
            if (Use_electricity <= Count_200kwh){
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_1));
                Electric_Charge = (int)(Use_electricity * L_CHARGE1);
                ReqUse_Charge = LReq_Charge(Base_Charge, Electric_Charge);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    if (Welfare == 0){
                        Discount += ReqUse_Charge;
                    }
                    Discount += Total_discount(Welfare_discount(Base_Charge + Electric_Charge)
                            , LargeFamily_discount(Base_Charge + Electric_Charge - ReqUse_Charge));

                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 저압 요금 : 201 ~ 400kWh 사용
                // 1단계(200kWh) + 2단계
            } else if (Use_electricity > Count_200kwh && Use_electricity <= Count_400kwh){
                Over_Charge = Use_electricity - Count_200kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_2));
                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Over_Charge * L_CHARGE2);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 저압 요금 : ~400kWh 사용
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계
            }else if (Use_electricity > Count_400kwh){
                Over_Charge = Use_electricity - Count_400kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_3));
                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Count_200kwh * L_CHARGE2) + (int)(Over_Charge * L_CHARGE3);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 저압 요금 : 1000kWh~ 사용 + 슈퍼유저
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계 + 6단계
            }else if (Use_electricity > Count_1000kwh && mSuper_user){
                Over_Charge = Use_electricity - Count_1000kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_3));
                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Count_200kwh * L_CHARGE2) + (int)((Count_200kwh + Count_400kwh) * L_CHARGE3) + (int)((Over_Charge) * L_CHARGE4);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
            }
        }else{
            // 고압 요금 : 200kWh 이하 사용
            // 1단계
            if (Use_electricity <= Count_200kwh){
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_1));
                Electric_Charge = (int)(Use_electricity * H_CHARGE1);
                ReqUse_Charge = HReq_Charge(Base_Charge, Electric_Charge);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    if (Welfare == 0){
                        Discount += ReqUse_Charge;
                    }
                    Discount += Total_discount(Welfare_discount(Base_Charge + Electric_Charge)
                            , LargeFamily_discount(Base_Charge + Electric_Charge - ReqUse_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 고압 요금 : 201 ~ 400kWh 사용
                // 1단계(200kWh) + 2단계
            } else if (Use_electricity > Count_200kwh && Use_electricity <= Count_400kwh){
                Over_Charge = Use_electricity - Count_200kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_2));
                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Over_Charge * H_CHARGE2);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount = Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 고압 요금 : ~400kWh 사용
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계
            }else if (Use_electricity > Count_400kwh){
                Over_Charge = Use_electricity - Count_400kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_3));
                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Count_200kwh * H_CHARGE2) + (int)(Over_Charge * H_CHARGE3);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount = Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 고압 요금 : 1000kWh~ 사용 + 슈퍼유저
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계 + 6단계
            }else if (Use_electricity > Count_1000kwh && mSuper_user){
                Over_Charge = Use_electricity - Count_1000kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_3));
                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Count_200kwh * H_CHARGE2) + (int)((Count_200kwh + Count_400kwh) * H_CHARGE3) + (int)((Over_Charge) * H_CHARGE4);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
            }
        }
        Log.d("기본요금", String.valueOf(Base_Charge));
        Log.d("전력량요금", String.valueOf(Electric_Charge));
        Log.d("필수사용량 할인", String.valueOf(ReqUse_Charge));
        Log.d("할인", String.valueOf(Discount));
        Log.d("청구금액", String.valueOf(Requisition_amount) + " 원");
        return Requisition_amount;
    }

    // 복지할인 계산
    private int Welfare_discount(int total){
        int Discount = 0;
        //0 - 해당없음
        //1-독립유공자, 2-국가유공자, 3-518민주운동자, 4-장애인, 5-사회복지시설
        //6-기초생활(생계, 의료), 7-기초생활(주거, 교육), 8-차상위계층
        if(Welfare == 0) {
            Discount = 0;
        }else if(Welfare == 1) {
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {

                Discount = total;
            }
        }else if ( Welfare == 2 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if ( Welfare == 3 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if ( Welfare == 4 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if(Welfare == 5){
            Discount = (int)(total*0.3);
        }else if ( Welfare == 6 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if(Welfare == 7){
            if (total > 10000){
                Discount = 10000;
            }else if( total <= 10000) {
                Discount = total;
            }
        }else if (Welfare == 8){
            if (total > 8000){
                Discount = 8000;
            }else if( total <= 8000) {
                Discount = total;
            }
        }
        Log.d("Welfare_discount", String.valueOf(Discount));
        return Discount;
    }

    // 대가족할인 계산
    private int LargeFamily_discount(int total){
        int Discount = 0;
        // 0 - 해당없음
        // 1-5인이상 가구, 2-출산가구, 3-3자녀이상가구
        // 4-생명유지장치
        if (LargeFamily == 0){
            Discount = 0;
        } else if (LargeFamily == 1 | LargeFamily == 2 | LargeFamily == 3){
            if (total * 0.3 < 16000){
                Discount = (int) (total * 0.3);
            }else if (total * 0.3 > 16000){
                Discount = 16000;
            }
        } else if (LargeFamily == 4){
            Discount = (int) (total * 0.3);
        }
        Log.d("LargeFamily_discount", String.valueOf(Discount));
        return Discount;
    }

    // 할인 총합계 계산
    private int Total_discount(int welfare_discount, int largeFamily_discount){
        int result = 0;
        if (welfare_discount - largeFamily_discount < 0){
            result = largeFamily_discount;
        } else {
            result = welfare_discount;
        }
        return result;
    }

    // 중복할인 계산
    private boolean double_discount(){
        // 기초생활(생계, 의료) || 기초생활(주거, 교육) || 차상위계층
        // 중복할인
        if (Welfare == 6 || Welfare == 7 || Welfare == 8){
            return true;
        }
        return false;
    }

    // 필수 사용량 보장공제(저압)
    private int LReq_Charge(int a, int b){
        // a : 기본요금
        // b : 전력량요금
        int result = 0;
        if (a + b - 4000 < 1000){
            result = a + b - 1000;
        }else if (a + b - 4000 > 1000){
            result = 4000;
        }
        return result;
    }

    // 필수 사용량 보장공제(고압)
    private int HReq_Charge(int a, int b){
        // a : 기본요금
        // b : 전력량요금
        int result = 0;
        if (a + b - 2500 < 1000){
            result = a + b - 1000;
        }else if (a + b - 2500 > 1000){
            result = 2500;
        }
        return result;
    }

    // 키보드 숨기기
    private void focusOff(View view){
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public class setDialog_Siren{
        public TextView txt_dialog_title;
        public ImageView img_1;
        public TextView txt_help;
        public TextView txt_tip;
        public Button btn_done;
    }
    public class setDialog_Max{
        public TextView txt_dialog_title;
        public TextView txt_help;
        public Spinner Max_Spinner;
        public Button btn_done;
        public Button btn_cancel;
    }
    public class setDialog_Detail{
        public TextView txt_dialog_title;
        public TextView txt_help;
        public TextView detail_Base_Charge;
        public TextView detail_Electric_Charge;
        public TextView detail_Total_Charge;
        public TextView detail_Total_Charge_add;;
        public TextView detail_Total_Charge_donate;
        public TextView detail_Requisition_amount;
        public Button btn_done;
    }
    public class setDialog_Version{
        public TextView txt_dialog_title;
        public ImageView img_1;
        public TextView txt_help;
        public Button btn_done;
    }
    public class setDialog_Quick{
        public TextView txt_dialog_title;
        public TextView txt_help;
        public TextView txt_total;
        public TextView txt_before;
        public EditText Total_electricity;
        public Button btn_done;
        public Button btn_cancel;
    }
}

