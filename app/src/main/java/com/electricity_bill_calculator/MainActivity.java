package com.electricity_bill_calculator;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.electricity_bill_calculator.DTO.MyElectricity;
import com.electricity_bill_calculator.Utils.CustomProgressDialog;
import com.electricity_bill_calculator.Utils.Electricity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.gson.Gson;
import com.tsengvn.typekit.TypekitContextWrapper;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.regex.Pattern;



public class MainActivity extends Activity implements View.OnClickListener{
    /*
      * setting : 내부 저장소
      * Welfare_item : 복지할인 항목
      * LargeFamily_item : 대가족 할인항목
      * save_check : 저장 체크
      * */
    public SharedPreferences settings;
    public int mWelfare_item, mLargeFamily_item;
    public int save_check = 0;
    public String Electricity_Pressure;
    public int Discount = 0;
    public int Requisition_amount;
    public int Base_Charge = 0;
    public int Total_Charge = 0;
    public int Electric_Charge = 0;
    public boolean mSuper_user;
    public boolean bIntent_analysis = false;
    private String ELECT;

    private EditText Total_electricity, Before_Month_electricity;
    private RadioButton Low_Pressure, High_Pressure;
    private Spinner sp_largeFamily, sp_welfare;
    private Button setting_saveButton;
    private ImageView help_Total_electricity, help_Before_Month_electricity, help_Super_user, back_button;
    private Switch super_user;

    private View HelpDialog;

    public static Electricity sElectricity;
    private LayoutInflater inflater;

    private ArrayList<MyElectricity> alMyElect = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 객체 연결
        setItem();

        sElectricity = new Electricity();
        settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);

        // 인텐트 데이터 읽기
        getIntent_Data();

        // 숫자 필터 설정
        setFilters();

        // Admob 연결
        setAd();

        // 확인버튼 연결
        setDoneButton();

        // 슈퍼유저
        setSuperuser();

    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    // 객체 연결
    private void setItem(){

        Before_Month_electricity = (EditText) findViewById(R.id.Before_Month_electricity);
        Total_electricity = (EditText) findViewById(R.id.Total_electricity);
        if (Before_Month_electricity.getText().toString().equals("")){
            Total_electricity.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        }else{
            Total_electricity.setImeOptions(EditorInfo.IME_ACTION_DONE);
        }
        Low_Pressure = (RadioButton) findViewById(R.id.Low_Pressure);
        High_Pressure = (RadioButton) findViewById(R.id.High_Pressure);
        sp_largeFamily = (Spinner) findViewById(R.id.sp_largeFamily);
        sp_welfare = (Spinner) findViewById(R.id.sp_welfare);
        setting_saveButton = (Button) findViewById(R.id.setting_saveButton);
        setting_saveButton.setOnClickListener(this);

        help_Total_electricity = (ImageView) findViewById(R.id.help_Total_electricity);
        help_Total_electricity.setOnClickListener(this);

        help_Before_Month_electricity = (ImageView) findViewById(R.id.help_Before_Month_electricity);
        help_Before_Month_electricity.setOnClickListener(this);

        help_Super_user = (ImageView) findViewById(R.id.help_Super_user);
        help_Super_user.setOnClickListener(this);

        super_user = (Switch) findViewById(R.id.super_user);
        back_button = (ImageView) findViewById(R.id.back_button);
        back_button.setOnClickListener(this);
    }

    // 숫자 필터 설정
    private void setFilters(){
        // 숫자 필터
        InputFilter filterNum = new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                Pattern ps = Pattern.compile("^[0-9]*$");
                if(!ps.matcher(source).matches()){
                    return  "";
                }
                return null;
            }
        };
        Total_electricity.setFilters(new InputFilter[] {filterNum});
        Total_electricity.setFilters(new InputFilter[] { new InputFilter.LengthFilter(5) }); //글자수 제한
        Before_Month_electricity.setFilters(new InputFilter[] {filterNum});
        Before_Month_electricity.setFilters(new InputFilter[] { new InputFilter.LengthFilter(5) }); //글자수 제한
    }

    // 복지할인 스피너 연결
    private void welfare_list_adapter(String item){
        final String[] Welfare_list = getResources().getStringArray(R.array.Welfare);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, Welfare_list);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        sp_welfare.setAdapter(adapter);
        sp_welfare.setSelection(Integer.parseInt(item));

        sp_welfare.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mWelfare_item = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // 대가족/생명유지장치 스피너 연결
    private void LargeFamily_list_adapter(String item){
        final String[] LargeFamily_list = getResources().getStringArray(R.array.LargeFamily);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, LargeFamily_list);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        sp_largeFamily.setAdapter(adapter);
        sp_largeFamily.setSelection(Integer.parseInt(item));

        sp_largeFamily.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mLargeFamily_item = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    // 설명 Dialog
    private void dialog_help(String title, int image, String context, boolean mSuper){
        inflater = getLayoutInflater();
        HelpDialog = inflater.inflate(R.layout.dialog_help1, null);
        setDialog setDialog = new setDialog();

        AlertDialog.Builder buider= new AlertDialog.Builder(this); //AlertDialog.Builder 객체 생성
        buider.setView(HelpDialog);

        setDialog.txt_dialog_title = (TextView) HelpDialog.findViewById(R.id.txt_dialog_title);
        setDialog.txt_dialog_title.setText(title);

        setDialog.img_1 = (ImageView) HelpDialog.findViewById(R.id.img_1);
        setDialog.img_1.setImageDrawable(getResources().getDrawable(image));

        if (mSuper) setDialog.img_1.setVisibility(View.GONE);

        setDialog.txt_help = (TextView) HelpDialog.findViewById(R.id.txt_help);
        setDialog.txt_help.setText(context);
        setDialog.btn_done = (Button) HelpDialog.findViewById(R.id.btn_done);

        final AlertDialog dialog = buider.create();
        setDialog.btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

    // 저장 (1)
    private void save_info(){
        save_check();
        SharedPreferences.Editor editor = settings.edit();
        if (save_check == 1) {
            Total_electricity.requestFocus();
            Total_electricity.setError("필수");
        }else if(save_check == 2) {
            Before_Month_electricity.requestFocus();
            Before_Month_electricity.setError("필수");
        }else if(save_check == 3) {
            Total_electricity.requestFocus();
            Total_electricity.setError("오류");
            Toast.makeText(this, "누적 사용량이 전월지침 보다 작습니다.", Toast.LENGTH_SHORT).show();
        }else if(save_check == 4) {
            Total_electricity.requestFocus();
            Total_electricity.setError("오류");
            Toast.makeText(this, "최대 4~5자리 숫자만 입력 가능합니다.", Toast.LENGTH_SHORT).show();
        }else if(save_check == 5) {
            Before_Month_electricity.requestFocus();
            Before_Month_electricity.setError("오류");
            Toast.makeText(this, "최대 4~5자리 숫자만 입력 가능합니다.", Toast.LENGTH_SHORT).show();
        }else if (save_check == 6){
            if (Low_Pressure.isChecked()) {
                Electricity_Pressure = "LOW";
            } else {
                Electricity_Pressure = "HIGH";
            }
            save_array();

            if (alMyElect.size() > 0) {
                boolean isChanged = false;
                for (int i=0; i<alMyElect.size(); i++) {
                    if (alMyElect.get(i).getDATE().equals(getDateString())) {
                        isChanged = true;
                        alMyElect.get(i).setCHANGE(Total_electricity.getText().toString());
                        alMyElect.get(i).setELECT(String.valueOf(getEstimated_charge()));
                    }
                }
                if (!isChanged) {
                    MyElectricity myElectricity = new MyElectricity(getDateString(), Total_electricity.getText().toString(), String.valueOf(getEstimated_charge()));
                    alMyElect.add(myElectricity);
                }
            } else {
                MyElectricity myElectricity = new MyElectricity(getDateString(), Total_electricity.getText().toString(), String.valueOf(getEstimated_charge()));
                alMyElect.add(myElectricity);
            }

            editor.putString("MyElectJson", new Gson().toJson(alMyElect));

            editor.putString("ELECT", sElectricity.modifyUsageElect(ELECT, Total_electricity.getText().toString()));
            editor.apply();
            Log.d("DataSave_Done", " 저장 완료");

            finish_main();
        }else if(save_check == 0){
            if (Low_Pressure.isChecked()) {
                Electricity_Pressure = "LOW";
            } else {
                Electricity_Pressure = "HIGH";
            }
            save_array();

            if (alMyElect.size() > 0) {
                boolean isChanged = false;
                for (int i=0; i<alMyElect.size(); i++) {
                    if (alMyElect.get(i).getDATE().equals(getDateString())) {
                        isChanged = true;
                        alMyElect.get(i).setCHANGE(Total_electricity.getText().toString());
                        alMyElect.get(i).setELECT(String.valueOf(getEstimated_charge()));
                    }
                }
                if (!isChanged) {
                    MyElectricity myElectricity = new MyElectricity(getDateString(), Total_electricity.getText().toString(), String.valueOf(getEstimated_charge()));
                    alMyElect.add(myElectricity);
                }
            } else {
                MyElectricity myElectricity = new MyElectricity(getDateString(), Total_electricity.getText().toString(), String.valueOf(getEstimated_charge()));
                alMyElect.add(myElectricity);
            }
            editor.putString("MyElectJson", new Gson().toJson(alMyElect));

            editor.putString("ELECT", sElectricity.addUsageElect(ELECT, Total_electricity.getText().toString()));
            editor.apply();
            Log.d("DataSave_Done", " 저장 완료");
            finish_main();
        }else{
            Snackbar.make(findViewById(android.R.id.content), "사용량 입력 실패.", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    // 저장 (2)
    private void save_array(){
        Requisition_amount = getEstimated_charge();

        SharedPreferences.Editor editor = settings.edit();

        editor.putString("Last_date", getDateString());
        editor.putString("Total_electricity", Total_electricity.getText().toString());
        editor.putString("Before_Month_electricity", Before_Month_electricity.getText().toString());
        editor.putString("Electricity_Pressure", Electricity_Pressure);
        editor.putInt("Electric_Charge", Electric_Charge);
        editor.putInt("Base_Charge", Base_Charge); // 기본요금
        editor.putInt("Total_Charge", Total_Charge); // 전기요금계
        editor.putInt("Welfare", mWelfare_item);
        editor.putInt("LargeFamily", mLargeFamily_item);
        editor.putInt("Requisition_amount", Requisition_amount);
        editor.putBoolean("mSuper_user", mSuper_user);
        editor.apply();


    }

    // 저장_체크
    private void save_check(){
        settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);
        ELECT = settings.getString("ELECT", "");

        if (Total_electricity.getText().toString().equals("")) {
            save_check = 1;
        }else if(Before_Month_electricity.getText().toString().equals("")){
            save_check = 2;
        }else if (Integer.parseInt(Total_electricity.getText().toString().trim())-Integer.parseInt(Before_Month_electricity.getText().toString().trim())<0) {
            save_check = 3;
        }else if (Total_electricity.getText().toString().length() > 5){
            save_check = 4;
        }else if (Before_Month_electricity.getText().toString().length() > 5) {
            save_check = 5;
        }else if (sElectricity.Electvalidate(ELECT) && !ELECT.equals("")){
            save_check = 6;
        }else{
            if (super_user.isChecked()){
                mSuper_user = true;
            }else{
                mSuper_user = false;
            }
            save_check = 0;
        }
    }

    // main activity 종료
    private void finish_main(){
        if (bIntent_analysis){
            setResult(1);
        }else{
            Intent intent = new Intent(this, HomeActivity.class);
            startActivity(intent);
        }
        finish();
    }

    // 현재 날짜 반환
    public String getDateString(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        return df.format(new Date());
    }

    // 인텐트 데이터 읽기
    private void getIntent_Data(){

        if (getIntent().getStringExtra("menu_item").equals("1")){
            Total_electricity.setHint(settings.getString("Total_electricity", "0"));
            Before_Month_electricity.setText(settings.getString("Before_Month_electricity", "0"));
            if (settings.getString("Electricity_Pressure", "LOW").equals("LOW")){
                Low_Pressure.setChecked(true);
            }else{
                High_Pressure.setChecked(true);
            }
            welfare_list_adapter(String.valueOf(settings.getInt("Welfare", 0)));
            LargeFamily_list_adapter(String.valueOf(settings.getInt("LargeFamily", 0)));
            back_button.setVisibility(View.VISIBLE);
            if (settings.getBoolean("mSuper_user", false)){
                super_user.setChecked(true);
            }else{
                super_user.setChecked(false);
            }
            bIntent_analysis = true;

        }else{
            welfare_list_adapter("0");
            LargeFamily_list_adapter("0");
            back_button.setVisibility(View.GONE);
            bIntent_analysis = false;
        }

        String myElect = settings.getString("MyElectJson", "");
        if (!myElect.equals("")) {
            try {
                JSONArray contacts = new JSONArray(myElect);

                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);
                    String DATE = c.getString("DATE");
                    String ELECT = c.getString("ELECT");
                    String CHANGE = c.getString("CHANGE");

                    MyElectricity m = new MyElectricity(DATE, ELECT, CHANGE);
                    alMyElect.add(m);
                }

            } catch (final JSONException e) {
                Log.e("MainActivity","Json parsing error: " + e.getMessage());
            }
        }
    }

    // 예상요금 계산
//    public int getEstimated_charge(){
//        int ReqUse_Charge = 0;
//
//        int Over_Charge = 0;
//        int Count_200kwh = 200;
//        int Count_400kwh = 400;
//        int Count_1000kwh = 1000;
//
//        int getTotal_electricity = Integer.parseInt(Total_electricity.getText().toString().trim());
//        int getBefore_Month_electricity = Integer.parseInt(Before_Month_electricity.getText().toString().trim());
//        int Use_electricity = getTotal_electricity - getBefore_Month_electricity;
//
//        float L_CHARGE1 = Float.parseFloat(getString(R.string.RC_LOW_1));
//        float L_CHARGE2 = Float.parseFloat(getString(R.string.RC_LOW_2));
//        float L_CHARGE3 = Float.parseFloat(getString(R.string.RC_LOW_3));
//        float L_CHARGE4 = Float.parseFloat(getString(R.string.RC_LOW_4));
//
//        float H_CHARGE1 = Float.parseFloat(getString(R.string.RC_HIGH_1));
//        float H_CHARGE2 = Float.parseFloat(getString(R.string.RC_HIGH_2));
//        float H_CHARGE3 = Float.parseFloat(getString(R.string.RC_HIGH_3));
//        float H_CHARGE4 = Float.parseFloat(getString(R.string.RC_HIGH_4));
//
//        float Industry_fund = Float.parseFloat(getString(R.string.Industry_fund));
//        float Added_tax = Float.parseFloat(getString(R.string.Added_tax));
//
//        if (Low_Pressure.isChecked()){
//            // 저압 요금 : 200kWh 이하 사용
//            // 1단계
//            if (Use_electricity <= Count_200kwh){
//                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_1));
//                Electric_Charge = (int)(Use_electricity * L_CHARGE1);
//                ReqUse_Charge = sElectricity.LReq_Charge(Base_Charge, Electric_Charge);
//
//                if (sElectricity.double_discount(mWelfare_item)){
//                    Discount = sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                            + sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), mWelfare_item);
//                }else{
//                    if (mWelfare_item == 0){
//                        Discount += ReqUse_Charge;
//                    }
//                    Discount += sElectricity.Total_discount(sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                           , sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - ReqUse_Charge, mLargeFamily_item));
//
//                }
//
//                Total_Charge = Base_Charge + Electric_Charge - Discount;
//                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
//
//            // 저압 요금 : 201 ~ 400kWh 사용
//            // 1단계(200kWh) + 2단계
//            } else if (Use_electricity > Count_200kwh && Use_electricity <= Count_400kwh){
//                Over_Charge = Use_electricity - Count_200kwh;
//                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_2));
//                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Over_Charge * L_CHARGE2);
//
//                if (sElectricity.double_discount(mWelfare_item)){
//                    Discount = sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                            + sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), mLargeFamily_item);
//                }else{
//                    Discount =  sElectricity.Total_discount(
//                            sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item),
//                            sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge, mLargeFamily_item));
//                }
//
//                Total_Charge = Base_Charge + Electric_Charge - Discount;
//                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
//
//            // 저압 요금 : ~400kWh 사용
//            // 1딘계(200kWh) + 2단계(200kWh) + 3단계
//            }else if (Use_electricity > Count_400kwh){
//                Over_Charge = Use_electricity - Count_400kwh;
//                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_3));
//                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Count_200kwh * L_CHARGE2) + (int)(Over_Charge * L_CHARGE3);
//
//                if (sElectricity.double_discount(mWelfare_item)){
//                    Discount = sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                            + sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), mLargeFamily_item);
//                }else{
//                    Discount =  sElectricity.Total_discount(sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge, mLargeFamily_item));
//                }
//
//                Total_Charge = Base_Charge + Electric_Charge - Discount;
//                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
//
//            // 저압 요금 : 1000kWh~ 사용 + 슈퍼유저
//            // 1딘계(200kWh) + 2단계(200kWh) + 3단계 + 6단계
//            }else if (Use_electricity > Count_1000kwh && super_user.isChecked()){
//                Over_Charge = Use_electricity - Count_1000kwh;
//                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_3));
//                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Count_200kwh * L_CHARGE2) + (int)((Count_200kwh + Count_400kwh) * L_CHARGE3) + (int)((Over_Charge) * L_CHARGE4);
//
//                if (sElectricity.double_discount(mWelfare_item)){
//                    Discount = sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                            + sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), mLargeFamily_item);
//                }else{
//                    Discount =  sElectricity.Total_discount(sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge, mLargeFamily_item));
//                }
//
//                Total_Charge = Base_Charge + Electric_Charge - Discount;
//                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
//            }
//        }else{
//            // 고압 요금 : 200kWh 이하 사용
//            // 1단계
//            if (Use_electricity <= Count_200kwh){
//                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_1));
//                Electric_Charge = (int)(Use_electricity * H_CHARGE1);
//                ReqUse_Charge = sElectricity.HReq_Charge(Base_Charge, Electric_Charge);
//
//                if (sElectricity.double_discount(mWelfare_item)){
//                    Discount = sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                            + sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), mLargeFamily_item);
//                }else{
//                    if (mWelfare_item == 0){
//                        Discount += ReqUse_Charge;
//                    }
//                    Discount += sElectricity.Total_discount(sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                            , sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - ReqUse_Charge, mLargeFamily_item));
//                }
//
//                Total_Charge = Base_Charge + Electric_Charge - Discount;
//                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
//
//            // 고압 요금 : 201 ~ 400kWh 사용
//            // 1단계(200kWh) + 2단계
//            } else if (Use_electricity > Count_200kwh && Use_electricity <= Count_400kwh){
//                Over_Charge = Use_electricity - Count_200kwh;
//                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_2));
//                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Over_Charge * H_CHARGE2);
//
//                if (sElectricity.double_discount(mWelfare_item)){
//                    Discount = sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                            + sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), mLargeFamily_item);
//                }else{
//                    Discount = sElectricity.Total_discount(sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge, mLargeFamily_item));
//                }
//
//                Total_Charge = Base_Charge + Electric_Charge - Discount;
//                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
//
//            // 고압 요금 : ~400kWh 사용
//            // 1딘계(200kWh) + 2단계(200kWh) + 3단계
//            }else if (Use_electricity > Count_400kwh){
//                Over_Charge = Use_electricity - Count_400kwh;
//                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_3));
//                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Count_200kwh * H_CHARGE2) + (int)(Over_Charge * H_CHARGE3);
//
//                if (sElectricity.double_discount(mWelfare_item)){
//                    Discount = sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                            + sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), mLargeFamily_item);
//                }else{
//                    Discount = sElectricity.Total_discount(sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge, mLargeFamily_item));
//                }
//
//                Total_Charge = Base_Charge + Electric_Charge - Discount;
//                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
//
//            // 고압 요금 : 1000kWh~ 사용 + 슈퍼유저
//            // 1딘계(200kWh) + 2단계(200kWh) + 3단계 + 6단계
//            }else if (Use_electricity > Count_1000kwh && super_user.isChecked()){
//                Over_Charge = Use_electricity - Count_1000kwh;
//                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_3));
//                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Count_200kwh * H_CHARGE2) + (int)((Count_200kwh + Count_400kwh) * H_CHARGE3) + (int)((Over_Charge) * H_CHARGE4);
//
//                if (sElectricity.double_discount(mWelfare_item)){
//                    Discount = sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item)
//                            + sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge - sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), mLargeFamily_item);
//                }else{
//                    Discount = sElectricity.Total_discount(sElectricity.Welfare_discount(Base_Charge + Electric_Charge, mWelfare_item), sElectricity.LargeFamily_discount(Base_Charge + Electric_Charge, mLargeFamily_item));
//                }
//
//                Total_Charge = Base_Charge + Electric_Charge - Discount;
//                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
//            }
//        }
//        Log.d("기본요금", String.valueOf(Base_Charge));
//        Log.d("전력량요금", String.valueOf(Electric_Charge));
//        Log.d("필수사용량 할인", String.valueOf(ReqUse_Charge));
//        Log.d("할인", String.valueOf(Discount));
//        Log.d("청구금액", String.valueOf(Requisition_amount) + " 원");
//        return Requisition_amount;
//    }

    public int getEstimated_charge(){
        int ReqUse_Charge = 0;

        int Over_Charge = 0;
        int Count_200kwh = 200;
        int Count_400kwh = 400;
        int Count_1000kwh = 1000;
        int Discount = 0;

        int getTotal_electricity = Integer.parseInt(Total_electricity.getText().toString().trim());
        int getBefore_Month_electricity = Integer.parseInt(Before_Month_electricity.getText().toString().trim());
        int Use_electricity = getTotal_electricity - getBefore_Month_electricity;

        float L_CHARGE1 = Float.parseFloat(getString(R.string.RC_LOW_1));
        float L_CHARGE2 = Float.parseFloat(getString(R.string.RC_LOW_2));
        float L_CHARGE3 = Float.parseFloat(getString(R.string.RC_LOW_3));
        float L_CHARGE4 = Float.parseFloat(getString(R.string.RC_LOW_4));

        float H_CHARGE1 = Float.parseFloat(getString(R.string.RC_HIGH_1));
        float H_CHARGE2 = Float.parseFloat(getString(R.string.RC_HIGH_2));
        float H_CHARGE3 = Float.parseFloat(getString(R.string.RC_HIGH_3));
        float H_CHARGE4 = Float.parseFloat(getString(R.string.RC_HIGH_4));

        float Industry_fund = Float.parseFloat(getString(R.string.Industry_fund));
        float Added_tax = Float.parseFloat(getString(R.string.Added_tax));

        if (Electricity_Pressure.equals("LOW")){
            // 저압 요금 : 200kWh 이하 사용
            // 1단계
            if (Use_electricity <= Count_200kwh){
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_1));
                Electric_Charge = (int)(Use_electricity * L_CHARGE1);
                ReqUse_Charge = LReq_Charge(Base_Charge, Electric_Charge);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    if (mWelfare_item == 0){
                        Discount += ReqUse_Charge;
                    }
                    Discount += Total_discount(Welfare_discount(Base_Charge + Electric_Charge)
                            , LargeFamily_discount(Base_Charge + Electric_Charge - ReqUse_Charge));

                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 저압 요금 : 201 ~ 400kWh 사용
                // 1단계(200kWh) + 2단계
            } else if (Use_electricity > Count_200kwh && Use_electricity <= Count_400kwh){
                Over_Charge = Use_electricity - Count_200kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_2));
                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Over_Charge * L_CHARGE2);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 저압 요금 : ~400kWh 사용
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계
            }else if (Use_electricity > Count_400kwh){
                Over_Charge = Use_electricity - Count_400kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_3));
                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Count_200kwh * L_CHARGE2) + (int)(Over_Charge * L_CHARGE3);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 저압 요금 : 1000kWh~ 사용 + 슈퍼유저
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계 + 6단계
            }else if (Use_electricity > Count_1000kwh && mSuper_user){
                Over_Charge = Use_electricity - Count_1000kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_3));
                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Count_200kwh * L_CHARGE2) + (int)((Count_200kwh + Count_400kwh) * L_CHARGE3) + (int)((Over_Charge) * L_CHARGE4);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
            }
        }else{
            // 고압 요금 : 200kWh 이하 사용
            // 1단계
            if (Use_electricity <= Count_200kwh){
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_1));
                Electric_Charge = (int)(Use_electricity * H_CHARGE1);
                ReqUse_Charge = HReq_Charge(Base_Charge, Electric_Charge);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    if (mWelfare_item == 0){
                        Discount += ReqUse_Charge;
                    }
                    Discount += Total_discount(Welfare_discount(Base_Charge + Electric_Charge)
                            , LargeFamily_discount(Base_Charge + Electric_Charge - ReqUse_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 고압 요금 : 201 ~ 400kWh 사용
                // 1단계(200kWh) + 2단계
            } else if (Use_electricity > Count_200kwh && Use_electricity <= Count_400kwh){
                Over_Charge = Use_electricity - Count_200kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_2));
                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Over_Charge * H_CHARGE2);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount = Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 고압 요금 : ~400kWh 사용
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계
            }else if (Use_electricity > Count_400kwh){
                Over_Charge = Use_electricity - Count_400kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_3));
                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Count_200kwh * H_CHARGE2) + (int)(Over_Charge * H_CHARGE3);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount = Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 고압 요금 : 1000kWh~ 사용 + 슈퍼유저
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계 + 6단계
            }else if (Use_electricity > Count_1000kwh && mSuper_user){
                Over_Charge = Use_electricity - Count_1000kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_3));
                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Count_200kwh * H_CHARGE2) + (int)((Count_200kwh + Count_400kwh) * H_CHARGE3) + (int)((Over_Charge) * H_CHARGE4);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
            }
        }
        Log.d("기본요금", String.valueOf(Base_Charge));
        Log.d("전력량요금", String.valueOf(Electric_Charge));
        Log.d("필수사용량 할인", String.valueOf(ReqUse_Charge));
        Log.d("할인", String.valueOf(Discount));
        Log.d("청구금액", String.valueOf(Requisition_amount) + " 원");
        return Requisition_amount;
    }

    // 복지할인 계산
    private int Welfare_discount(int total){
        int Discount = 0;
        //0 - 해당없음
        //1-독립유공자, 2-국가유공자, 3-518민주운동자, 4-장애인, 5-사회복지시설
        //6-기초생활(생계, 의료), 7-기초생활(주거, 교육), 8-차상위계층
        if(mWelfare_item == 0) {
            Discount = 0;
        }else if(mWelfare_item == 1) {
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {

                Discount = total;
            }
        }else if ( mWelfare_item == 2 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if ( mWelfare_item == 3 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if ( mWelfare_item == 4 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if(mWelfare_item == 5){
            Discount = (int)(total*0.3);
        }else if ( mWelfare_item == 6 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if(mWelfare_item == 7){
            if (total > 10000){
                Discount = 10000;
            }else if( total <= 10000) {
                Discount = total;
            }
        }else if (mWelfare_item == 8){
            if (total > 8000){
                Discount = 8000;
            }else if( total <= 8000) {
                Discount = total;
            }
        }
        Log.d("Welfare_discount", String.valueOf(Discount));
        return Discount;
    }

    // 대가족할인 계산
    private int LargeFamily_discount(int total){
        int Discount = 0;
        // 0 - 해당없음
        // 1-5인이상 가구, 2-출산가구, 3-3자녀이상가구
        // 4-생명유지장치
        if (mLargeFamily_item == 0){
            Discount = 0;
        } else if (mLargeFamily_item == 1 | mLargeFamily_item == 2 | mLargeFamily_item == 3){
            if (total * 0.3 < 16000){
                Discount = (int) (total * 0.3);
            }else if (total * 0.3 > 16000){
                Discount = 16000;
            }
        } else if (mLargeFamily_item == 4){
            Discount = (int) (total * 0.3);
        }
        Log.d("LargeFamily_discount", String.valueOf(Discount));
        return Discount;
    }

    // 할인 총합계 계산
    private int Total_discount(int welfare_discount, int largeFamily_discount){
        int result = 0;
        if (welfare_discount - largeFamily_discount < 0){
            result = largeFamily_discount;
        } else {
            result = welfare_discount;
        }
        return result;
    }

    // 중복할인 계산
    private boolean double_discount(){
        // 기초생활(생계, 의료) || 기초생활(주거, 교육) || 차상위계층
        // 중복할인
        if (mWelfare_item == 6 || mWelfare_item == 7 || mWelfare_item == 8){
            return true;
        }
        return false;
    }

    // 필수 사용량 보장공제(저압)
    private int LReq_Charge(int a, int b){
        // a : 기본요금
        // b : 전력량요금
        int result = 0;
        if (a + b - 4000 < 1000){
            result = a + b - 1000;
        }else if (a + b - 4000 > 1000){
            result = 4000;
        }
        return result;
    }

    // 필수 사용량 보장공제(고압)
    private int HReq_Charge(int a, int b){
        // a : 기본요금
        // b : 전력량요금
        int result = 0;
        if (a + b - 2500 < 1000){
            result = a + b - 1000;
        }else if (a + b - 2500 > 1000){
            result = 2500;
        }
        return result;
    }


    // Admob 연결
    private void setAd(){
        MobileAds.initialize(getApplicationContext(), getString(R.string.banner_ad_unit_id));

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .build();
        mAdView.loadAd(adRequest);
    }

    // Edittext 확인 버튼 처리
    private void setDoneButton(){
        Total_electricity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE){
                    if (Total_electricity.getText().toString().trim().equals("") ||
                            Total_electricity.getText().toString().trim().equals(null)){
                        Total_electricity.setError("필수");
                    }else if(!Before_Month_electricity.getText().toString().trim().equals("")){
                        focusOff(Total_electricity);
                        setSwitch();
                    }
                    return true;
                }else{
                    return false;
                }
            }
        });
        Before_Month_electricity.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE){
                    focusOff(Before_Month_electricity);
                    if (Total_electricity.getText().toString().trim().equals("") ||
                            Total_electricity.getText().toString().trim().equals(null)){
                        Total_electricity.setError("필수");
                    }else if (Before_Month_electricity.getText().toString().trim().equals("") ||
                            Before_Month_electricity.getText().toString().trim().equals(null)){
                        Before_Month_electricity.setError("필수");
                    }else{
                        focusOff(Before_Month_electricity);
                        setSwitch();
                    }
                    return true;
                }
                return false;
            }
        });
    }

    // 키보드 숨기기
    private void focusOff(View view){
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    // 슈퍼유저 세팅
    private void setSwitch(){
        if (checkSuperMonth()){
            super_user.setChecked(true);
            Toast.makeText(this, "슈퍼유저요금이 적용되었습니다.", Toast.LENGTH_SHORT).show();
        }else{
            super_user.setChecked(false);
        }
    }
    private void setSuperuser(){
        super_user.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    if (Total_electricity.getText().toString().trim().equals("")){
                        focusOff(Total_electricity);
                        Total_electricity.setError("필수");
                        super_user.setChecked(false);
                    }else if (Before_Month_electricity.getText().toString().trim().equals("")) {
                        focusOff(Before_Month_electricity);
                        Before_Month_electricity.setError("필수");
                        super_user.setChecked(false);
                    }else if (!checkSuperMonth()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        AlertDialog dialog = builder.setTitle("알림")
                                .setMessage("슈퍼유저요금 대상자가 아닙니다.")
                                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        super_user.setChecked(false);
                                    }
                                })
                                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .create();
                        dialog.show();
                    }
                } else if (checkSuperMonth()){
                    AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                    AlertDialog dialog = builder.setTitle("알림")
                            .setMessage("슈퍼유저요금 대상자입니다.")
                            .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    super_user.setChecked(true);
                                }
                            })
                            .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create();
                    dialog.show();
                } else {
                    if (checkSuperMonth()){
                        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
                        AlertDialog dialog = builder.setTitle("알림")
                                .setMessage("슈퍼유저요금 대상자입니다.\n적용 하시겠습니까?")
                                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        super_user.setChecked(true);
                                    }
                                })
                                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                })
                                .create();
                        dialog.show();
                    }
                }
            }
        });
    }
    private boolean checkSuperMonth(){
        SimpleDateFormat df = new SimpleDateFormat("MM", Locale.KOREA);
        String str_date = df.format(new Date());
        boolean result = false;
        if (Total_electricity.getText().toString().trim().equals("")){
            focusOff(Total_electricity);
            Total_electricity.setError("필수");
        }else if (Before_Month_electricity.getText().toString().trim().equals("")) {
            focusOff(Before_Month_electricity);
            Before_Month_electricity.setError("필수");
        }else if (!Total_electricity.getText().toString().trim().equals("") && !Before_Month_electricity.getText().toString().trim().equals("")){
            int total = Integer.parseInt(Total_electricity.getText().toString().trim());
            int before = Integer.parseInt(Before_Month_electricity.getText().toString().trim());

            if ((str_date.equals("07") || str_date.equals("08") || str_date.equals("12") || str_date.equals("02"))
                    && total-before > 1000) {
                result = true;
            }
        }

        return result;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.help_Total_electricity:
                dialog_help("누적 사용량", R.drawable.img_1, "전기계량기의 숫자 중 소수점 이하를\n제외한 숫자 입니다.", false);
                break;
            case R.id.help_Before_Month_electricity:
                dialog_help("전월지침", R.drawable.img_2, "지난달까지의 사용량이며,\n지난달 전기요금 고지서의 당월지침 숫자 입니다.\n\n또한 한국전력공사 홈페이지에서 확인 가능합니다.", false);
                break;
            case R.id.help_Super_user:
                dialog_help("슈퍼유저요금", R.drawable.img_1, "동·하계(7~8월, 12~2월)\n\n사용 전력량 1,000kWh 초과시,n전력량요금 최고 단계 적용", true);
                break;
            case R.id.setting_saveButton:
                save_info();
                break;
            case R.id.back_button:
                finish();
                break;
        }
    }

    public class setDialog{
        public TextView txt_dialog_title;
        public ImageView img_1;
        public TextView txt_help;
        public Button btn_done;
    }
}
