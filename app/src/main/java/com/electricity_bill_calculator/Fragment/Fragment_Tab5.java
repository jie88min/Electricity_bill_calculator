package com.electricity_bill_calculator.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.electricity_bill_calculator.Adapter.Adapter_Elect_Recycler;
import com.electricity_bill_calculator.DTO.MyElectricity;
import com.electricity_bill_calculator.R;
import com.electricity_bill_calculator.Utils.Electricity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.electricity_bill_calculator.AnalysisActivity.ELECT;

public class Fragment_Tab5 extends Fragment {

    private Unbinder unbinder;
    private SharedPreferences settings;

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.term_sp) Spinner term_sp;
    @BindView(R.id.empty) TextView empty;

    private Electricity electricity;
    private ArrayList<MyElectricity> MyElectricity;
    private Adapter_Elect_Recycler adapter;

    private String ELECT;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab5, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public Fragment newInstance(String tab05) {
        Fragment_Tab5 f = new Fragment_Tab5();

        Bundle args = new Bundle();
        args.putString("tab", tab05);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onResume() {
        super.onResume();

        settings = getActivity().getSharedPreferences("settings", Activity.MODE_PRIVATE);

        ELECT = settings.getString("ELECT", "");
        MyElectricity = new ArrayList<>();
        electricity = new Electricity();

        adapter = new Adapter_Elect_Recycler(getActivity(), MyElectricity, R.layout.elect_list);

        electricity.getUsageElect(ELECT, MyElectricity, adapter);

        if (MyElectricity.size() > 0) {
            recyclerView.setAdapter(adapter);
            empty.setVisibility(View.GONE);
        } else {
            empty.setVisibility(View.VISIBLE);
        }

        ArrayList<String> term1 = new ArrayList<>();
        term1.add("전체");
        term1.add(electricity.getMonthString() + "월");
        term1.add(String.valueOf(electricity.getBeforeMonthString(1) + "월"));
        term1.add(String.valueOf(electricity.getBeforeMonthString(2) + "월"));

        ArrayAdapter<String> term = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, term1);
        term_sp.setAdapter(term);

        term_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        MyElectricity.clear();
                        electricity.getUsageElect(ELECT, MyElectricity, adapter);
                        break;
                    case 1:
                        MyElectricity.clear();
                        electricity.getUsageElect(ELECT, MyElectricity, adapter, electricity.getMonthString());
                        break;
                    case 2:
                        MyElectricity.clear();
                        electricity.getUsageElect(ELECT, MyElectricity, adapter, electricity.getBeforeMonthString(1));
                        break;
                    case 3:
                        MyElectricity.clear();
                        electricity.getUsageElect(ELECT, MyElectricity, adapter, electricity.getBeforeMonthString(2));
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(linearLayoutManager);
    }
}
