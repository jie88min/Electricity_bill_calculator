package com.electricity_bill_calculator.Fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.electricity_bill_calculator.Adapter.Adapter_Tips_Recycler;
import com.electricity_bill_calculator.R;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class Fragment_Tab2 extends Fragment {

    @BindView(R.id.tipListview) RecyclerView tipListview;
    @BindView(R.id.adView) AdView adView;

    private Unbinder unbinder;
    public SharedPreferences settings;

    private ArrayList<String> alTips = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab2, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setAd();

        alTips.add("사용량을 자주 수정해 줄 수록 정확한 요금을 알 수 있습니다.");
        alTips.add("냉장고 문을 잠깐만 열었다 닫아도 전력 소모가 커 문을 자주 열지만 않아도 그만큼 전기를 아낄 수 있죠.");
        alTips.add("냉장고의 적정 온도를 맞추는 것도 중요합니다. 특히 보관하는 내용물이 많을수록 온도유지를 위해 전력이 낭비될 수도 있으니 주의하세요!");
        alTips.add("집집마다 놓여있는 TV의 전력소모량도 무시할 수 없습니다. 자체 스위치를 꺼도 플러그가 꼽혀 있다면 일정량의 대기전력이 소모되기 때문이죠.");
        alTips.add("책상 위에 놓여있는 컴퓨터 역시 불필요하게 소모되는 전력이 만만치 않습니다. 항상 꽂아둔 USB와 화려한 스크린세이버는 전력소비에 탁월하죠.");
        alTips.add("에어컨의 경우 처음 가동할 때 가장 많은 전기가 소모되므로 일단 강하게 튼 뒤 점차 온도를 맞추는 것이 좋습니다.");
        alTips.add("전기밥솥의 보온기능은 전기요금 상승의 주범!");
        alTips.add("청소기는 필터의 먼지만 자주 제거해 주셔도 전력 소모를 줄이는데 큰 효과를 보실 수 있습니다.");

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        tipListview.setLayoutManager(linearLayoutManager);

        Adapter_Tips_Recycler adapter_tips_recycler = new Adapter_Tips_Recycler(getActivity(), alTips, R.layout.item_tips);
        tipListview.setAdapter(adapter_tips_recycler);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public Fragment newInstance(String tab02) {
        Fragment_Tab2 f = new Fragment_Tab2();

        Bundle args = new Bundle();
        args.putString("tab", tab02);
        f.setArguments(args);

        return f;
    }

    // Admob 연결
    private void setAd(){
        MobileAds.initialize(getActivity().getApplicationContext(), getString(R.string.banner_ad_unit_id));

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest);
    }

}
