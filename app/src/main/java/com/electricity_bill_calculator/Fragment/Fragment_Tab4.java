package com.electricity_bill_calculator.Fragment;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.BuildConfig;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.electricity_bill_calculator.Dialog.Dialog_Max_Charge;
import com.electricity_bill_calculator.Dialog.Dialog_Version;
import com.electricity_bill_calculator.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

import java.text.NumberFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class Fragment_Tab4 extends Fragment {

    @BindView(R.id.versionView) LinearLayout versionView;
    @BindView(R.id.tvVersion) TextView tvVersion;
    @BindView(R.id.tvUpdate) TextView tvUpdate;
    @BindView(R.id.maxChargeView) LinearLayout maxChargeView;
    @BindView(R.id.tvMaxCharge) TextView tvMaxCharge;
    @BindView(R.id.shareView) LinearLayout shareView;
    @BindView(R.id.reviewView) LinearLayout reviewView;

    private Unbinder unbinder;
    private SharedPreferences settings;

    private Dialog_Max_Charge dialog_max_charge;

    private FirebaseRemoteConfig mFirebaseRemoteConfig;
    private static final String APP_VERSION_KEY = "app_version";
    private String REMOTE_APP_VERSION = "";
    private String CURRUNT_APP_VERSION = "";

    Dialog_Version dialog_version;
    private boolean update_able = false;
    private int dialog_result = 0;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();
        mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
        mFirebaseRemoteConfig.setConfigSettings(configSettings);
        mFirebaseRemoteConfig.setDefaults(R.xml.remote_config_defaults);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab4, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public Fragment newInstance(String tab04) {
        Fragment_Tab4 f = new Fragment_Tab4();

        Bundle args = new Bundle();
        args.putString("tab", tab04);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        settings = getActivity().getSharedPreferences("settings", Activity.MODE_PRIVATE);

        setVersion();
        setTvMaxCharge();



    }

    private void setVersion() {

        try {
            PackageInfo i = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);
            CURRUNT_APP_VERSION = i.versionName;
        } catch(PackageManager.NameNotFoundException ignored) { }

        tvVersion.setText(CURRUNT_APP_VERSION);

        long cacheExpiration = 0;
        mFirebaseRemoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            mFirebaseRemoteConfig.activateFetched();
                            REMOTE_APP_VERSION = mFirebaseRemoteConfig.getString(APP_VERSION_KEY);

                            String dialog_msg = "최신버전 입니다!";
                            String dialog_btn = "확인";
                            if (!CURRUNT_APP_VERSION.equals(REMOTE_APP_VERSION)) {
                                dialog_msg = "업데이트를 할 수 있어요!\n업데이트 하시겠어요?";
                                dialog_btn = "업데이트";
                                update_able = true;
                                if (tvUpdate != null) {
                                    tvUpdate.setVisibility(View.VISIBLE);
                                }
                            } else {
                                if (tvUpdate != null) {
                                    tvUpdate.setVisibility(View.GONE);
                                }
                            }

                            dialog_version = new Dialog_Version(getActivity(), dialog_msg, dialog_btn, dialog_result);
                            dialog_version.setOnDismissListener(new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {
                                    if (update_able && dialog_version.getResult() == 1) {
                                        Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
                                        marketLaunch.setData(Uri
                                                .parse("https://play.google.com/store/apps/details?id=com.electricity_bill_calculator"));
                                        startActivity(marketLaunch);
                                    }
                                }
                            });
                        }
                    }
                });


    }

    private void setTvMaxCharge() {
        String max = settings.getString("mMax_charge", "50000");
        NumberFormat nf = NumberFormat.getInstance();
        tvMaxCharge.setText(nf.format(Integer.valueOf(max)) + "원");
    }

    @OnClick(R.id.versionView)
    void setBtnVersion() {
        if (dialog_version != null) {
            dialog_result = 0;
            dialog_version.show();
        }
    }

    @OnClick(R.id.maxChargeView)
    void setBtnMax() {
        dialog_max_charge = new Dialog_Max_Charge(getActivity());
        dialog_max_charge.show();

        dialog_max_charge.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                setTvMaxCharge();
            }
        });
    }

    @OnClick(R.id.shareView)
    void setBtnShare() {
        Intent intent = new Intent(Intent.ACTION_SEND);

        intent.setType("text/plain");

        String text = "당신의 전기요금을 지켜드립니다. \n https://play.google.com/store/apps/details?id=com.electricity_bill_calculator";
        intent.putExtra(Intent.EXTRA_TEXT, text);
        Intent chooser = Intent.createChooser(intent, "친구에게 공유하기");
        startActivity(chooser);
    }

    @OnClick(R.id.reviewView)
    void setBtnReview() {
        Intent marketLaunch = new Intent(Intent.ACTION_VIEW);
        marketLaunch.setData(Uri
                .parse("https://play.google.com/store/apps/details?id=com.electricity_bill_calculator"));
        startActivity(marketLaunch);
    }


}
