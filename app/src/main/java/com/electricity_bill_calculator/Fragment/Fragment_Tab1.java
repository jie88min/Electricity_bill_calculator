package com.electricity_bill_calculator.Fragment;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.electricity_bill_calculator.Dialog.Dialog_Detail_Price;
import com.electricity_bill_calculator.MainActivity;
import com.electricity_bill_calculator.R;
import com.electricity_bill_calculator.Utils.CircularProgressBar;
import com.electricity_bill_calculator.Utils.Electricity;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class Fragment_Tab1 extends Fragment {

    @BindView(R.id.circularProgress)
    CircularProgressBar circularProgress;
    @BindView(R.id.tvTotal) TextView tvTotal;
    @BindView(R.id.tvUse) TextView tvUse;
    @BindView(R.id.tvTax) TextView tvTax;
    @BindView(R.id.ivSiren) ImageView ivSiren;
    @BindView(R.id.tvSiren) TextView tvSiren;
    @BindView(R.id.btnUseChange) RelativeLayout btnUseChange;
    @BindView(R.id.adView) AdView adView;
    @BindView(R.id.totalPriveView) LinearLayout totalPriveView;
    @BindView(R.id.taxView) LinearLayout taxView;
    @BindView(R.id.tvMessage) TextView tvMessage;

    private Unbinder unbinder;
    public SharedPreferences settings;

    public static int Requisition_amount=0, Welfare=0, LargeFamily=0, Electric_Charge=0;
    private String Total_electricity="", Before_Month_electricity="", Electricity_Pressure="", Last_date="";
    public static String ELECT = "";
    private int mMax_charge=0, sp_max=0;
    public int Base_Charge = 0;
    public int Total_Charge = 0;
    public int persent=0;
    private Electricity electricity;

    private boolean mSuper_user = false;
    private boolean toggle = false;
    private boolean progressive = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tab1, container, false);
        unbinder = ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setAd();

    }

    @Override
    public void onResume() {
        super.onResume();

        getData();
        getEstimated_charge(Total_electricity);
        setInfomation();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public Fragment newInstance(String tab01) {
        Fragment_Tab1 f = new Fragment_Tab1();

        Bundle args = new Bundle();
        args.putString("tab", tab01);
        f.setArguments(args);

        return f;
    }

    @OnClick(R.id.totalPriveView)
    void setBtnTotalPriveView() {
        Dialog_Detail_Price dialog_detail_price = new Dialog_Detail_Price(
                getActivity(), "계산 내역", Base_Charge, Electric_Charge, Total_Charge, Requisition_amount
        );
        dialog_detail_price.show();
    }

    @OnClick(R.id.taxView)
    void setBtnTaxView() {
        tvMessage.setText(electricity.progressive_tax(Integer.parseInt(Total_electricity) - Integer.parseInt(Before_Month_electricity)));
    }

    @OnClick(R.id.btnUseChange)
    void setBtnUseChange() {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtra("menu_item", "1");
        startActivity(intent);
    }

    // Admob 연결
    private void setAd(){
        MobileAds.initialize(getActivity().getApplicationContext(), getString(R.string.banner_ad_unit_id));

        AdRequest adRequest = new AdRequest.Builder()
                .build();
        adView.loadAd(adRequest);
    }

    private void setInfomation() {

//        tvMessage.setText(getDateString());

        String str = String.format("%,d", Math.round(Requisition_amount));
        tvTotal.setText(str + "원");

        String str1 = String.format("%,d", Math.round(Integer.parseInt(Total_electricity) - Integer.parseInt(Before_Month_electricity)));
        tvUse.setText(str1);

        setSiren();

        tvSiren.setText(persent + "%");

        setProgress();

        electricity = new Electricity();
        electricity.progressive_tax(Integer.parseInt(Total_electricity) - Integer.parseInt(Before_Month_electricity));

        tvMessage.setText(electricity.progressive_tax(Integer.parseInt(Total_electricity) - Integer.parseInt(Before_Month_electricity)));

        if (electricity.progressivetax == 0){
            tvTax.setText("미적용");
        } else if (electricity.progressivetax == 1){
            tvTax.setText("1단계");
        } else if (electricity.progressivetax == 2) {
            tvTax.setText("2단계");
            tvTax.setTextColor(getResources().getColor(R.color.colorYellow));
        }
    }

    // 데이터 읽기
    private void getData(){
        settings = getActivity().getSharedPreferences("settings", Activity.MODE_PRIVATE);

        Total_electricity = settings.getString("Total_electricity", "0");
        Before_Month_electricity = settings.getString("Before_Month_electricity", "0");
        Electricity_Pressure = settings.getString("Electricity_Pressure", "LOW");
        mMax_charge = Integer.parseInt(settings.getString("mMax_charge", "100000"));
        Last_date = settings.getString("Last_date", getDateString());
        Welfare = settings.getInt("Welfare", 0);
        LargeFamily = settings.getInt("LargeFamily", 0);
        Requisition_amount = settings.getInt("Requisition_amount", 0);
        Base_Charge = settings.getInt("Base_Charge", 0);
        Total_Charge = settings.getInt("Total_Charge", 0);
        Electric_Charge = settings.getInt("Electric_Charge", 0);
        mSuper_user = settings.getBoolean("mSuper_user", false);
        ELECT = settings.getString("ELECT", "");
    }

    // 현재 날짜 반환
    public String getDateString(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        String str_date = df.format(new Date());

        return str_date;
    }

    private void setProgress(){
        circularProgress.setProgressColor(getResources().getColor(R.color.colorBrown2));
        circularProgress.setTextColor(getResources().getColor(R.color.colorBrown2));
        circularProgress.setProgressWidth(15);
        circularProgress.setNum(Requisition_amount);
        circularProgress.setmMax_charge(mMax_charge);
        circularProgress.setProgress(persent);
    }

    private void setSiren(){
        persent = (int)(((double)Requisition_amount/(double)mMax_charge)*100);
        if (persent < 40) {
            ivSiren.setImageDrawable(getResources().getDrawable(R.drawable.alarm_green));
        }else if (persent >= 40 && persent < 80){
            ivSiren.setImageDrawable(getResources().getDrawable(R.drawable.alarm_warning));
        }else{
            ivSiren.setImageDrawable(getResources().getDrawable(R.drawable.alarm_red));
        }
    }

    // 예상요금 계산
    public int getEstimated_charge(String Total_electricity){
        int ReqUse_Charge = 0;

        int Over_Charge = 0;
        int Count_200kwh = 200;
        int Count_400kwh = 400;
        int Count_1000kwh = 1000;
        int Discount = 0;

        int getTotal_electricity = Integer.parseInt(Total_electricity);
        int getBefore_Month_electricity = Integer.parseInt(Before_Month_electricity);
        int Use_electricity = getTotal_electricity - getBefore_Month_electricity;

        float L_CHARGE1 = Float.parseFloat(getString(R.string.RC_LOW_1));
        float L_CHARGE2 = Float.parseFloat(getString(R.string.RC_LOW_2));
        float L_CHARGE3 = Float.parseFloat(getString(R.string.RC_LOW_3));
        float L_CHARGE4 = Float.parseFloat(getString(R.string.RC_LOW_4));

        float H_CHARGE1 = Float.parseFloat(getString(R.string.RC_HIGH_1));
        float H_CHARGE2 = Float.parseFloat(getString(R.string.RC_HIGH_2));
        float H_CHARGE3 = Float.parseFloat(getString(R.string.RC_HIGH_3));
        float H_CHARGE4 = Float.parseFloat(getString(R.string.RC_HIGH_4));

        float Industry_fund = Float.parseFloat(getString(R.string.Industry_fund));
        float Added_tax = Float.parseFloat(getString(R.string.Added_tax));

        if (Electricity_Pressure.equals("LOW")){
            // 저압 요금 : 200kWh 이하 사용
            // 1단계
            if (Use_electricity <= Count_200kwh){
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_1));
                Electric_Charge = (int)(Use_electricity * L_CHARGE1);
                ReqUse_Charge = LReq_Charge(Base_Charge, Electric_Charge);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    if (Welfare == 0){
                        Discount += ReqUse_Charge;
                    }
                    Discount += Total_discount(Welfare_discount(Base_Charge + Electric_Charge)
                            , LargeFamily_discount(Base_Charge + Electric_Charge - ReqUse_Charge));

                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 저압 요금 : 201 ~ 400kWh 사용
                // 1단계(200kWh) + 2단계
            } else if (Use_electricity > Count_200kwh && Use_electricity <= Count_400kwh){
                Over_Charge = Use_electricity - Count_200kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_2));
                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Over_Charge * L_CHARGE2);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 저압 요금 : ~400kWh 사용
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계
            }else if (Use_electricity > Count_400kwh){
                Over_Charge = Use_electricity - Count_400kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_3));
                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Count_200kwh * L_CHARGE2) + (int)(Over_Charge * L_CHARGE3);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 저압 요금 : 1000kWh~ 사용 + 슈퍼유저
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계 + 6단계
            }else if (Use_electricity > Count_1000kwh && mSuper_user){
                Over_Charge = Use_electricity - Count_1000kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_LOW_3));
                Electric_Charge = (int)(Count_200kwh * L_CHARGE1) + (int)(Count_200kwh * L_CHARGE2) + (int)((Count_200kwh + Count_400kwh) * L_CHARGE3) + (int)((Over_Charge) * L_CHARGE4);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
            }
        }else{
            // 고압 요금 : 200kWh 이하 사용
            // 1단계
            if (Use_electricity <= Count_200kwh){
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_1));
                Electric_Charge = (int)(Use_electricity * H_CHARGE1);
                ReqUse_Charge = HReq_Charge(Base_Charge, Electric_Charge);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    if (Welfare == 0){
                        Discount += ReqUse_Charge;
                    }
                    Discount += Total_discount(Welfare_discount(Base_Charge + Electric_Charge)
                            , LargeFamily_discount(Base_Charge + Electric_Charge - ReqUse_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 고압 요금 : 201 ~ 400kWh 사용
                // 1단계(200kWh) + 2단계
            } else if (Use_electricity > Count_200kwh && Use_electricity <= Count_400kwh){
                Over_Charge = Use_electricity - Count_200kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_2));
                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Over_Charge * H_CHARGE2);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount = Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 고압 요금 : ~400kWh 사용
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계
            }else if (Use_electricity > Count_400kwh){
                Over_Charge = Use_electricity - Count_400kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_3));
                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Count_200kwh * H_CHARGE2) + (int)(Over_Charge * H_CHARGE3);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount = Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;

                // 고압 요금 : 1000kWh~ 사용 + 슈퍼유저
                // 1딘계(200kWh) + 2단계(200kWh) + 3단계 + 6단계
            }else if (Use_electricity > Count_1000kwh && mSuper_user){
                Over_Charge = Use_electricity - Count_1000kwh;
                Base_Charge = Integer.parseInt(getString(R.string.R_HIGH_3));
                Electric_Charge = (int)(Count_200kwh * H_CHARGE1) + (int)(Count_200kwh * H_CHARGE2) + (int)((Count_200kwh + Count_400kwh) * H_CHARGE3) + (int)((Over_Charge) * H_CHARGE4);

                if (double_discount()){
                    Discount = Welfare_discount(Base_Charge + Electric_Charge)
                            + LargeFamily_discount(Base_Charge + Electric_Charge - Welfare_discount(Base_Charge + Electric_Charge));
                }else{
                    Discount =  Total_discount(Welfare_discount(Base_Charge + Electric_Charge), LargeFamily_discount(Base_Charge + Electric_Charge));
                }

                Total_Charge = Base_Charge + Electric_Charge - Discount;
                Requisition_amount = ((Total_Charge + (int)(Total_Charge * Added_tax) + (int)(Total_Charge * Industry_fund))/10)*10;
            }
        }
        Log.d("기본요금", String.valueOf(Base_Charge));
        Log.d("전력량요금", String.valueOf(Electric_Charge));
        Log.d("필수사용량 할인", String.valueOf(ReqUse_Charge));
        Log.d("할인", String.valueOf(Discount));
        Log.d("청구금액", String.valueOf(Requisition_amount) + " 원");
        return Requisition_amount;
    }

    // 복지할인 계산
    private int Welfare_discount(int total){
        int Discount = 0;
        //0 - 해당없음
        //1-독립유공자, 2-국가유공자, 3-518민주운동자, 4-장애인, 5-사회복지시설
        //6-기초생활(생계, 의료), 7-기초생활(주거, 교육), 8-차상위계층
        if(Welfare == 0) {
            Discount = 0;
        }else if(Welfare == 1) {
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {

                Discount = total;
            }
        }else if ( Welfare == 2 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if ( Welfare == 3 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if ( Welfare == 4 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if(Welfare == 5){
            Discount = (int)(total*0.3);
        }else if ( Welfare == 6 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if(Welfare == 7){
            if (total > 10000){
                Discount = 10000;
            }else if( total <= 10000) {
                Discount = total;
            }
        }else if (Welfare == 8){
            if (total > 8000){
                Discount = 8000;
            }else if( total <= 8000) {
                Discount = total;
            }
        }
        Log.d("Welfare_discount", String.valueOf(Discount));
        return Discount;
    }

    // 대가족할인 계산
    private int LargeFamily_discount(int total){
        int Discount = 0;
        // 0 - 해당없음
        // 1-5인이상 가구, 2-출산가구, 3-3자녀이상가구
        // 4-생명유지장치
        if (LargeFamily == 0){
            Discount = 0;
        } else if (LargeFamily == 1 | LargeFamily == 2 | LargeFamily == 3){
            if (total * 0.3 < 16000){
                Discount = (int) (total * 0.3);
            }else if (total * 0.3 > 16000){
                Discount = 16000;
            }
        } else if (LargeFamily == 4){
            Discount = (int) (total * 0.3);
        }
        Log.d("LargeFamily_discount", String.valueOf(Discount));
        return Discount;
    }

    // 할인 총합계 계산
    private int Total_discount(int welfare_discount, int largeFamily_discount){
        int result = 0;
        if (welfare_discount - largeFamily_discount < 0){
            result = largeFamily_discount;
        } else {
            result = welfare_discount;
        }
        return result;
    }

    // 중복할인 계산
    private boolean double_discount(){
        // 기초생활(생계, 의료) || 기초생활(주거, 교육) || 차상위계층
        // 중복할인
        if (Welfare == 6 || Welfare == 7 || Welfare == 8){
            return true;
        }
        return false;
    }

    // 필수 사용량 보장공제(저압)
    private int LReq_Charge(int a, int b){
        // a : 기본요금
        // b : 전력량요금
        int result = 0;
        if (a + b - 4000 < 1000){
            result = a + b - 1000;
        }else if (a + b - 4000 > 1000){
            result = 4000;
        }
        return result;
    }

    // 필수 사용량 보장공제(고압)
    private int HReq_Charge(int a, int b){
        // a : 기본요금
        // b : 전력량요금
        int result = 0;
        if (a + b - 2500 < 1000){
            result = a + b - 1000;
        }else if (a + b - 2500 > 1000){
            result = 2500;
        }
        return result;
    }

}
