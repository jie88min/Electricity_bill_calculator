package com.electricity_bill_calculator.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.electricity_bill_calculator.DTO.MyElectricity;
import com.electricity_bill_calculator.R;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.CandleEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.utils.MPPointF;
import com.github.mikephil.charting.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class Fragment_Tab3 extends SimpleFragment implements OnChartGestureListener {


    private Unbinder unbinder;
    private SharedPreferences settings;

    @BindView(R.id.mChart) LineChart mChart;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.emptyChart) TextView emptyChart;
    @BindView(R.id.detail_Base_Charge) TextView detail_Base_Charge;
    @BindView(R.id.detail_Electric_Charge) TextView detail_Electric_Charge;
    @BindView(R.id.detail_Total_Charge) TextView detail_Total_Charge;
    @BindView(R.id.detail_Total_Charge_add) TextView detail_Total_Charge_add;
    @BindView(R.id.detail_Total_Charge_donate) TextView detail_Total_Charge_donate;
    @BindView(R.id.detail_Requisition_amount) TextView detail_Requisition_amount;
    @BindView(R.id.btnDaily) Button btnDaily;
    @BindView(R.id.btnMonth) Button btnMonth;

    private ArrayList<MyElectricity> alMyElect = new ArrayList<>();

    public static int Requisition_amount=0, Welfare=0, LargeFamily=0, Electric_Charge=0;
    private String Total_electricity="", Before_Month_electricity="", Electricity_Pressure="", Last_date="";
    public static String ELECT = "";
    private int mMax_charge=0, sp_max=0;
    public int Base_Charge = 0;
    public int Total_Charge = 0;
    public int persent=0;
    private boolean mSuper_user = false;

    private int graph_kind = 1;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab3, container, false);
        unbinder = ButterKnife.bind(this, view);
        return view;
    }

    public Fragment newInstance(String tab03) {
        Fragment_Tab3 f = new Fragment_Tab3();

        Bundle args = new Bundle();
        args.putString("tab", tab03);
        f.setArguments(args);

        return f;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onResume() {
        super.onResume();
        getData();
        setInit();

        setGraph();
    }

    @OnClick(R.id.btnDaily)
    void setBtnDaily() {
        if (graph_kind != 1) {
            graph_kind = 1;
            setBtn(graph_kind);
            setGraph();
        }
    }

    @OnClick(R.id.btnMonth)
    void setBtnMonth() {
        if (graph_kind != 2) {
            graph_kind = 2;
            setBtn(graph_kind);
            setGraph();
        }
    }

    private void setBtn(int position) {
        btnMonth.setBackgroundResource(R.drawable.shape_right_rounded_gray);
        btnDaily.setBackgroundResource(R.drawable.shape_left_rounded_gray);

        if (position == 1) {
            btnDaily.setBackgroundResource(R.drawable.shape_left_rounded_brown);
        } else if (position == 2) {
            btnMonth.setBackgroundResource(R.drawable.shape_right_rounded_brown);
        }
    }

    private void setGraph() {
        if (alMyElect.size() > 0) {
            // create a new chart object
            mChart.setPadding(10, 10, 10, 10);

            List<Entry> entries = new ArrayList<>();
            ArrayList<MyElectricity> temp = getData(graph_kind, alMyElect);

            for (int i=0; i<temp.size(); i++) {
                Log.d("getData", temp.get(i).getDATE());
                Log.d("getData", temp.get(i).getELECT());
                Log.d("getData", temp.get(i).getCHANGE());
                entries.add(new Entry(i, Integer.valueOf(temp.get(i).getCHANGE())));
            }

            LineDataSet lineDataSet = new LineDataSet(entries, "전기요금");
            lineDataSet.setLineWidth(2);
            lineDataSet.setCircleRadius(6);
            lineDataSet.setCircleColor(Color.parseColor("#795548"));
            lineDataSet.setCircleColorHole(Color.parseColor("#5D4037"));
            lineDataSet.setColor(Color.parseColor("#795548"));
            lineDataSet.setDrawCircleHole(true);
            lineDataSet.setDrawCircles(true);
            lineDataSet.setDrawHorizontalHighlightIndicator(false);
            lineDataSet.setDrawHighlightIndicators(false);
            lineDataSet.setDrawValues(false);

            LineData lineData = new LineData(lineDataSet);
            mChart.setData(lineData);

            XAxis xAxis = mChart.getXAxis();
            xAxis.setValueFormatter(new myXAisValueFormatter(temp));
            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setTextColor(Color.BLACK);
            xAxis.enableGridDashedLine(8, 24, 0);

            YAxis yLAxis = mChart.getAxisLeft();
            yLAxis.setTextColor(Color.BLACK);

            YAxis yRAxis = mChart.getAxisRight();
            yRAxis.setDrawLabels(false);
            yRAxis.setDrawAxisLine(false);
            yRAxis.setDrawGridLines(false);

            Description description = new Description();
            description.setText("");

            MyMarkerView marker = new MyMarkerView(getActivity(), R.layout.chartmarker);
            marker.setChartView(mChart);
            mChart.setMarker(marker);

            mChart.setDoubleTapToZoomEnabled(false);
            mChart.setDrawGridBackground(false);
            mChart.setDescription(description);
            mChart.animateY(1000, Easing.EasingOption.EaseInCubic);
            mChart.invalidate();

            mChart.setVisibility(View.VISIBLE);
            emptyChart.setVisibility(View.GONE);
        } else {
            mChart.setVisibility(View.GONE);
            emptyChart.setVisibility(View.VISIBLE);
        }
    }

    class myXAisValueFormatter implements IAxisValueFormatter {

        private ArrayList<MyElectricity> mValues;

        public myXAisValueFormatter(ArrayList<MyElectricity> mValues) {
            this.mValues = mValues;
        }

        @Override
        public String getFormattedValue(float value, AxisBase axis) {
            //20180101
            if (value < mValues.size()) {
                return mValues.get((int)value).getDATE().substring(5).replace("-","/");
            } else {
                return "";
            }

        }
    }

    class MyMarkerView extends MarkerView {

        private TextView tvContent;

        public MyMarkerView(Context context, int layoutResource) {
            super(context, layoutResource);

            tvContent = findViewById(R.id.tvContent);
        }

        // callbacks everytime the MarkerView is redrawn, can be used to update the
        // content (user-interface)
        @Override
        public void refreshContent(Entry e, Highlight highlight) {

            if (e instanceof CandleEntry) {

                CandleEntry ce = (CandleEntry) e;

                tvContent.setText("" + Utils.formatNumber(ce.getHigh(), 0, true).replace(".",",") + "원");
            } else {

                tvContent.setText("" + Utils.formatNumber(e.getY(), 0, true).replace(".",",") + "원");
            }

            super.refreshContent(e, highlight);
        }

        @Override
        public MPPointF getOffset() {
            return new MPPointF(-(getWidth() / 2), -getHeight());
        }
    }

    private void setDummy() {
        MyElectricity d2 = new MyElectricity("2018-01-01", "3343","12612");
        MyElectricity d3 = new MyElectricity("2018-03-02", "3444","19412");
        MyElectricity d4 = new MyElectricity("2018-03-12", "3444","22412");
        MyElectricity d5 = new MyElectricity("2018-06-02", "3555","17412");
        MyElectricity d6 = new MyElectricity("2018-06-04", "3555","19512");
        MyElectricity d7 = new MyElectricity("2018-09-04", "3666","10412");
        MyElectricity d8 = new MyElectricity("2018-12-02", "3777","22412");

        alMyElect.add(d2);
        alMyElect.add(d3);
        alMyElect.add(d4);
        alMyElect.add(d5);
        alMyElect.add(d6);
        alMyElect.add(d7);
        alMyElect.add(d8);

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "START");
    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {
        Log.i("Gesture", "END");
        mChart.highlightValues(null);
    }

    @Override
    public void onChartLongPressed(MotionEvent me) {
        Log.i("LongPress", "Chart longpressed.");
    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {
        Log.i("DoubleTap", "Chart double-tapped.");
    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
        Log.i("SingleTap", "Chart single-tapped.");
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {
        Log.i("Fling", "Chart flinged. VeloX: " + velocityX + ", VeloY: " + velocityY);
    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {
        Log.i("Scale / Zoom", "ScaleX: " + scaleX + ", ScaleY: " + scaleY);
    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {
        Log.i("Translate / Move", "dX: " + dX + ", dY: " + dY);
    }

    // 현재 날짜 반환
    public String getDateString(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        String str_date = df.format(new Date());

        return str_date;
    }

    private void getData(){
        settings = getActivity().getSharedPreferences("settings", Activity.MODE_PRIVATE);

        Total_electricity = settings.getString("Total_electricity", "0");
        Before_Month_electricity = settings.getString("Before_Month_electricity", "0");
        Electricity_Pressure = settings.getString("Electricity_Pressure", "LOW");
        mMax_charge = Integer.parseInt(settings.getString("mMax_charge", "100000"));
        Last_date = settings.getString("Last_date", getDateString());
        Welfare = settings.getInt("Welfare", 0);
        LargeFamily = settings.getInt("LargeFamily", 0);
        Requisition_amount = settings.getInt("Requisition_amount", 0);
        Base_Charge = settings.getInt("Base_Charge", 0);
        Total_Charge = settings.getInt("Total_Charge", 0);
        Electric_Charge = settings.getInt("Electric_Charge", 0);
        mSuper_user = settings.getBoolean("mSuper_user", false);
        ELECT = settings.getString("ELECT", "");
    }

    private void setInit() {
        settings = getActivity().getSharedPreferences("settings", Activity.MODE_PRIVATE);
        alMyElect.clear();

        String myElect = settings.getString("MyElectJson", "");
        Log.d("Fragment_Tab3", "myElect: " + myElect);
        if (!myElect.equals("")) {
            try {
                JSONArray contacts = new JSONArray(myElect);

                for (int i = 0; i < contacts.length(); i++) {
                    JSONObject c = contacts.getJSONObject(i);
                    String DATE = c.getString("DATE");
                    String ELECT = c.getString("ELECT");
                    String CHANGE = c.getString("CHANGE");

                    MyElectricity m = new MyElectricity(DATE, ELECT, CHANGE);
                    alMyElect.add(m);
                }

            } catch (final JSONException e) {
                Log.e("MainActivity","Json parsing error: " + e.getMessage());
            }
        }

        tvDate.setText(getDateString());
        detail_Base_Charge.setText(String.format("%,d", Math.round(Base_Charge)) + "원");
        detail_Electric_Charge.setText(String.format("%,d", Math.round(Electric_Charge)) + "원");
        detail_Total_Charge.setText(String.format("%,d", Math.round(Total_Charge)) + "원");
        detail_Total_Charge_add.setText(String.format("%,d", Math.round(Total_Charge * 0.1)) + "원");
        detail_Total_Charge_donate.setText(String.format("%,d", Math.round(Total_Charge * 0.037)) + "원");
        detail_Requisition_amount.setText(String.format("%,d", Math.round(Requisition_amount)) + "원");
    }

    static class Compare implements Comparator<MyElectricity> {

        @Override
        public int compare(MyElectricity o1, MyElectricity o2) {
            return o1.getDATE().compareTo(o2.getDATE());
        }
    }

    private ArrayList<MyElectricity> getData(int kind, ArrayList<MyElectricity> source) {

        ArrayList<MyElectricity> temp = new ArrayList<>();
        String TAG = "getData";
        Collections.sort(source, new Compare());
        if (kind == 1) {
            // 일별
            temp = source;
        } else if (kind == 2) {
            // 월별

            for (int i=0; i<source.size(); i++) {
                boolean isAdded = false;
                String year_month = source.get(i).getDATE().substring(0, 7);
                Log.d(TAG, "temp.size() : " + String.valueOf(temp.size()));
                for (int j=0; j<temp.size(); j++) {
                    if (temp.get(j).getDATE().substring(0, 7).equals(year_month)) {
                        if (Integer.valueOf(temp.get(j).getDATE().substring(8)) < Integer.valueOf(source.get(i).getDATE().substring(8))) {
                            temp.set(j, source.get(i));
                            isAdded = true;
                        }
                    }
                }
                if (!isAdded) {
                    Log.d(TAG, "Add" + source.get(i).getDATE());
                    temp.add(source.get(i));
                }
            }
        }
        return temp;
    }

}
