package com.electricity_bill_calculator.Utils;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;

import com.electricity_bill_calculator.R;

/**
 * Created by 김지민 on 2017-07-21.
 */

public class CustomProgressDialog extends Dialog {
    public CustomProgressDialog(Context context) {
        super(context);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.progress_dialog);
    }
}
