package com.electricity_bill_calculator.Utils.ChartView.draw.data;

/**
 * Created by 김지민 on 2018-07-27.
 */

public class DrawData {

    private int startX;
    private int startY;

    private int stopX;
    private int stopY;

    public int getStartX() {
        return startX;
    }

    public void setStartX(int startX) {
        this.startX = startX;
    }

    public int getStartY() {
        return startY;
    }

    public void setStartY(int startY) {
        this.startY = startY;
    }

    public int getStopX() {
        return stopX;
    }

    public void setStopX(int stopX) {
        this.stopX = stopX;
    }

    public int getStopY() {
        return stopY;
    }

    public void setStopY(int stopY) {
        this.stopY = stopY;
    }
}