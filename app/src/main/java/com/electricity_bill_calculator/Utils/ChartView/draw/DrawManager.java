package com.electricity_bill_calculator.Utils.ChartView.draw;

/**
 * Created by 김지민 on 2018-07-27.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.support.annotation.NonNull;

import com.electricity_bill_calculator.Utils.ChartView.animation.data.AnimationValue;
import com.electricity_bill_calculator.Utils.ChartView.draw.data.Chart;

public class DrawManager {

    private DrawController controller;
    private Chart chart;

    public DrawManager(@NonNull Context context) {
        chart = new Chart();
        controller = new DrawController(context, chart);
    }

    public Chart chart() {
        return chart;
    }

    public void updateTitleWidth() {
        controller.updateTitleWidth();
    }

    public void draw(@NonNull Canvas canvas) {
        controller.draw(canvas);
    }

    public void updateValue(@NonNull AnimationValue value) {
        controller.updateValue(value);
    }
}