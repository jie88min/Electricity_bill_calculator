package com.electricity_bill_calculator.Utils.ChartView.animation.data;

/**
 * Created by 김지민 on 2018-07-27.
 */

public class AnimationValue {

    private int x;
    private int y;
    private int alpha;
    private int runningAnimationPosition;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public int getRunningAnimationPosition() {
        return runningAnimationPosition;
    }

    public void setRunningAnimationPosition(int runningAnimationPosition) {
        this.runningAnimationPosition = runningAnimationPosition;
    }
}