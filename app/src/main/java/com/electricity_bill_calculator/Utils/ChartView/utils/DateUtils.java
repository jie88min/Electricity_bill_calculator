package com.electricity_bill_calculator.Utils.ChartView.utils;

/**
 * Created by 김지민 on 2018-07-27.
 */

import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateUtils {

    public static String format(long millis){
        SimpleDateFormat format = new SimpleDateFormat("dd", Locale.getDefault());
        return format.format(millis);
    }
}