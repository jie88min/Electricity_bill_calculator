package com.electricity_bill_calculator.Utils;

import android.content.SharedPreferences;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.electricity_bill_calculator.Adapter.Adapter_Elect_Recycler;
import com.electricity_bill_calculator.DTO.MyElectricity;
import com.electricity_bill_calculator.MyElectricityListAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by 김지민 on 2017-07-20.
 */

public class Electricity {
    private String Elect;
    public SharedPreferences settings;
    private int BASE_USE = 0;
    private int Change;

    public int progressivetax = 0;

    public Electricity(){}

    public boolean validate(String elect, String day){
        if (elect.contains(day)){
            return false;
        }
        return true;
    }

    public String progressive_tax(int tax){
        String result;

        if (tax <= 200){
            result = "앞으로 " + (200 - tax) + " kWh 더 사용 시\n1단계 누진세가 적용됩니다.";
            progressivetax = 0;
        } else if (tax > 200 && tax <= 400){
            result = "앞으로 " + (400 - tax) + " kWh 더 사용 시\n2단계 누진세가 적용됩니다.";
            progressivetax = 1;
        } else {
            result = "현재 2단계 누진세 적용중 입니다.";
            progressivetax = 2;
        }

        return result;
    }

    public boolean Electvalidate(String elect){
        if (elect.contains(getDateString())){
            return true;
        } else {
            return false;
        }
    }
    //[2017-08-08:18889]
    public void getUsageElect(View view, String elect, List<MyElectricity> myElectricities, MyElectricityListAdapter adapter){
        int temp = 0;
        int startpoint = temp;
        int endpoint = temp;
        int count = 0;
        String date = "", use = "", change = "";
        BASE_USE = 0;
        if (!elect.equals("")){
            for (int i = temp; i < elect.length(); i++){
                if (elect.charAt(i) == '['){
                    startpoint = i + 1;
                }

                if (elect.charAt(i) == ':'){
                    endpoint = i;
                    date = elect.substring(startpoint, endpoint);

                    startpoint = endpoint + 1;
                }

                if (elect.charAt(i) == ']'){
                    endpoint = i;
                    use = elect.substring(startpoint, endpoint);
                    Change = Integer.parseInt(use);

                    if (BASE_USE == 0) {
                        BASE_USE = Change;
                        change = "0";
                    } else {
                        change = String.valueOf(Change - BASE_USE);
                        BASE_USE = Change;
                    }

                    MyElectricity myElectricity = new MyElectricity(date, use, change);
                    myElectricities.add(myElectricity);
                    count++;

                    if (count == 0){
                        Snackbar.make(view, "기록이 없습니다.", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }
            }
            Collections.reverse(myElectricities);
            adapter.notifyDataSetChanged();
        }
    }

    public void getUsageElect(View view, String elect, List<MyElectricity> myElectricities, MyElectricityListAdapter adapter, String term){
        int temp = 0;
        int startpoint = temp;
        int endpoint = temp;
        int count = 0;
        String date = "", use = "", change = "";
        BASE_USE = 0;

        if (!elect.equals("")){
            for (int i = temp; i < elect.length(); i++){
                if (elect.charAt(i) == '['){
                    startpoint = i + 1;
                }

                if (elect.charAt(i) == ':'){
                    endpoint = i;
                    date = elect.substring(startpoint, endpoint);

                    startpoint = endpoint + 1;
                }

                if (elect.charAt(i) == ']'){
                    endpoint = i;
                    use = elect.substring(startpoint, endpoint);
                    Change = Integer.parseInt(use);

                    if (BASE_USE == 0) {
                        BASE_USE = Change;
                        change = "0";
                    } else {
                        change = String.valueOf(Change - BASE_USE);
                        BASE_USE = Change;
                    }

                    if (SelectMonth(date).equals(term)){
                        MyElectricity myElectricity = new MyElectricity(date, use, change);

                        myElectricities.add(myElectricity);
                        count++;
                    }
                    if (count == 0){
                        Snackbar.make(view, "기록이 없습니다.", Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                    }
                }
            }
            Collections.reverse(myElectricities);
            adapter.notifyDataSetChanged();
        }
    }

    public void getUsageElect(String elect, ArrayList<MyElectricity> myElectricities, Adapter_Elect_Recycler adapter){
        int temp = 0;
        int startpoint = temp;
        int endpoint = temp;
        int count = 0;
        String date = "", use = "", change = "";
        BASE_USE = 0;
        if (!elect.equals("")){
            for (int i = temp; i < elect.length(); i++){
                if (elect.charAt(i) == '['){
                    startpoint = i + 1;
                }

                if (elect.charAt(i) == ':'){
                    endpoint = i;
                    date = elect.substring(startpoint, endpoint);

                    startpoint = endpoint + 1;
                }

                if (elect.charAt(i) == ']'){
                    endpoint = i;
                    use = elect.substring(startpoint, endpoint);
                    Change = Integer.parseInt(use);

                    if (BASE_USE == 0) {
                        BASE_USE = Change;
                        change = "0";
                    } else {
                        change = String.valueOf(Change - BASE_USE);
                        BASE_USE = Change;
                    }

                    MyElectricity myElectricity = new MyElectricity(date, use, change);
                    myElectricities.add(myElectricity);
                    count++;
                }
            }
            Collections.reverse(myElectricities);
            adapter.notifyDataSetChanged();
        }
    }

    public void getUsageElect(String elect, ArrayList<MyElectricity> myElectricities, Adapter_Elect_Recycler adapter, String term){
        int temp = 0;
        int startpoint = temp;
        int endpoint = temp;
        int count = 0;
        String date = "", use = "", change = "";
        BASE_USE = 0;

        if (!elect.equals("")){
            for (int i = temp; i < elect.length(); i++){
                if (elect.charAt(i) == '['){
                    startpoint = i + 1;
                }

                if (elect.charAt(i) == ':'){
                    endpoint = i;
                    date = elect.substring(startpoint, endpoint);

                    startpoint = endpoint + 1;
                }

                if (elect.charAt(i) == ']'){
                    endpoint = i;
                    use = elect.substring(startpoint, endpoint);
                    Change = Integer.parseInt(use);

                    if (BASE_USE == 0) {
                        BASE_USE = Change;
                        change = "0";
                    } else {
                        change = String.valueOf(Change - BASE_USE);
                        BASE_USE = Change;
                    }

                    if (SelectMonth(date).equals(term)){
                        MyElectricity myElectricity = new MyElectricity(date, use, change);

                        myElectricities.add(myElectricity);
                        count++;
                    }
                }
            }
            Collections.reverse(myElectricities);
            adapter.notifyDataSetChanged();
        }
    }

    // 현재 날짜 반환
    public String getDateString(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        String str_date = df.format(new Date());

        return str_date;
    }

    public String getBeforeMonthString(int before){
        Calendar cal = new GregorianCalendar(Locale.KOREA);
        cal.setTime(new Date());
        cal.add(Calendar.MONTH, -before);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        String str_date = df.format(cal.getTime());

        return str_date.substring(5, 7);
    }

    public String getMonthString(){
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREA);
        String str_date = df.format(new Date());

        return str_date.substring(5, 7);
    }

    public String SelectMonth(String month){
        return month.substring(5, 7);
    }

    public String addUsageElect(String elect, String use){
        return elect + "[" + getDateString() + ":" + use + "]";
    }

    public String modifyUsageElect(String elect, String change){
        int temp = 0;
        int startpoint = temp;
        int endpoint = temp;
        String day = getDateString();

        String first = "";
        String modify = "";
        String end = "";

        endpoint = elect.indexOf(day);

        first = elect.substring(startpoint, endpoint - 1);

        modify = "[" + day + ":" + change + "]";

        for (int i = endpoint; i < elect.length(); i++){
            if (elect.charAt(i) == ']'){
                startpoint = i + 1;
            }
        }
        endpoint = elect.length();
        end = elect.substring(startpoint, endpoint);

        return first + modify + end;
    }

    // 복지할인 계산
    public int Welfare_discount(int total, int Welfare_item){
        int Discount = 0;
        //0 - 해당없음
        //1-독립유공자, 2-국가유공자, 3-518민주운동자, 4-장애인, 5-사회복지시설
        //6-기초생활(생계, 의료), 7-기초생활(주거, 교육), 8-차상위계층
        if(Welfare_item == 0) {
            Discount = 0;
        }else if(Welfare_item == 1) {
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {

                Discount = total;
            }
        }else if ( Welfare_item == 2 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if ( Welfare_item == 3 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if ( Welfare_item == 4 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if(Welfare_item == 5){
            Discount = (int)(total*0.3);
        }else if ( Welfare_item == 6 ){
            if (total > 16000) {
                Discount = 16000;
            } else if (total <= 16000) {
                Discount = total;
            }
        }else if(Welfare_item == 7){
            if (total > 10000){
                Discount = 10000;
            }else if( total <= 10000) {
                Discount = total;
            }
        }else if (Welfare_item == 8){
            if (total > 8000){
                Discount = 8000;
            }else if( total <= 8000) {
                Discount = total;
            }
        }
        return Discount;
    }

    // 대가족할인 계산
    public int LargeFamily_discount(int total, int LargeFamily_item){
        int Discount = 0;
        // 0 - 해당없음
        // 1-5인이상 가구, 2-출산가구, 3-3자녀이상가구
        // 4-생명유지장치
        if (LargeFamily_item == 0){
            Discount = 0;
        } else if (LargeFamily_item == 1 | LargeFamily_item == 2 | LargeFamily_item == 3){
            if (total * 0.3 < 16000){
                Discount = (int) (total * 0.3);
            }else if (total * 0.3 > 16000){
                Discount = 16000;
            }
        } else if (LargeFamily_item == 4){
            Discount = (int) (total * 0.3);
        }
        return Discount;
    }

    // 할인 총합계 계산
    public int Total_discount(int welfare_discount, int largeFamily_discount){
        if (welfare_discount - largeFamily_discount < 0){
            return largeFamily_discount;
        } else {
            return welfare_discount;
        }
    }

    // 중복할인 계산
    public boolean double_discount(int Welfare_item){
        // 기초생활(생계, 의료) || 기초생활(주거, 교육) || 차상위계층
        // 중복할인
        if (Welfare_item == 6 || Welfare_item == 7 || Welfare_item == 8){
            return true;
        }
        return false;
    }

    // 필수 사용량 보장공제(저압)
    public int LReq_Charge(int a, int b){
        // a : 기본요금
        // b : 전력량요금
        int result = 0;
        if (a + b - 4000 < 1000){
            result = a + b - 1000;
        }else if (a + b - 4000 > 1000){
            result = 4000;
        }
        return result;
    }

    // 필수 사용량 보장공제(고압)
    public int HReq_Charge(int a, int b){
        // a : 기본요금
        // b : 전력량요금
        int result = 0;
        if (a + b - 2500 < 1000){
            result = a + b - 1000;
        }else if (a + b - 2500 > 1000){
            result = 2500;
        }
        return result;
    }

}
