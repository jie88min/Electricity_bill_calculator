package com.electricity_bill_calculator.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.electricity_bill_calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kimjimin on 2018. 7. 29..
 */

public class Dialog_Version extends Dialog{

    private Dialog dialog;

    @BindView(R.id.tvMessage) TextView tvMessage;
    @BindView(R.id.btnDone) Button btn_done;

    private String dialog_title;
    private String btn_message;
    private int result = 0;

    public Dialog_Version(@NonNull Context context, String dialog_title, String btn_message, int result) {
        super(context);
        this.dialog_title = dialog_title;
        this.btn_message = btn_message;
        this.result = result;
    }

    public Dialog getDialog() {
        return dialog;
    }

    public int getResult() {
        return result;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_version);
        ButterKnife.bind(this);

        dialog = this;

        getDialog().setCanceledOnTouchOutside(true);
        setCancelable(true);

        tvMessage.setText(dialog_title);

        btn_done.setText(btn_message);

    }

    @OnClick(R.id.btnDone)
    void setBtnDone() {
        getDialog().dismiss();
        result = 1;
    }
}
