package com.electricity_bill_calculator.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.electricity_bill_calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kimjimin on 2018. 7. 29..
 */

public class Dialog_Detail_Price extends Dialog{

    private Dialog dialog;

    @BindView(R.id.txt_dialog_title) TextView txt_dialog_title;
    @BindView(R.id.detail_Base_Charge) TextView detail_Base_Charge;
    @BindView(R.id.detail_Electric_Charge) TextView detail_Electric_Charge;
    @BindView(R.id.detail_Total_Charge) TextView detail_Total_Charge;
    @BindView(R.id.detail_Total_Charge_add) TextView detail_Total_Charge_add;
    @BindView(R.id.detail_Total_Charge_donate) TextView detail_Total_Charge_donate;
    @BindView(R.id.detail_Requisition_amount) TextView detail_Requisition_amount;
    @BindView(R.id.btn_done) Button btn_done;

    private String dialog_title;
    private int str_detail_Base_Charge;
    private int str_detail_Electric_Charge;
    private int str_detail_Total_Charge;
    private int str_detail_Requisition_amount;

    public Dialog_Detail_Price(@NonNull Context context, String dialog_title, int str_detail_Base_Charge,
                               int str_detail_Electric_Charge, int str_detail_Total_Charge, int str_detail_Requisition_amount) {
        super(context);
        this.dialog_title = dialog_title;
        this.str_detail_Base_Charge = str_detail_Base_Charge;
        this.str_detail_Electric_Charge = str_detail_Electric_Charge;
        this.str_detail_Total_Charge = str_detail_Total_Charge;
        this.str_detail_Requisition_amount = str_detail_Requisition_amount;
    }

    public Dialog getDialog() {
        return dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_detail);
        ButterKnife.bind(this);

        dialog = this;

        getDialog().setCanceledOnTouchOutside(true);
        setCancelable(true);

        txt_dialog_title.setText(dialog_title);

        detail_Base_Charge.setText(String.format("%,d", Math.round(str_detail_Base_Charge)) + "원");
        detail_Electric_Charge.setText(String.format("%,d", Math.round(str_detail_Electric_Charge)) + "원");
        detail_Total_Charge.setText(String.format("%,d", Math.round(str_detail_Total_Charge)) + "원");
        detail_Total_Charge_add.setText(String.format("%,d", Math.round(str_detail_Total_Charge * 0.1)) + "원");
        detail_Total_Charge_donate.setText(String.format("%,d", Math.round(str_detail_Total_Charge * 0.037)) + "원");
        detail_Requisition_amount.setText(String.format("%,d", Math.round(str_detail_Requisition_amount)) + "원");

    }

    @OnClick(R.id.btn_done)
    void setBtnDone() {
        getDialog().dismiss();
    }
}
