package com.electricity_bill_calculator.Dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.electricity_bill_calculator.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by kimjimin on 2018. 7. 29..
 */

public class Dialog_Max_Charge extends Dialog{

    public SharedPreferences settings;
    private Context context;

    private Dialog dialog;

    @BindView(R.id.Max_Spinner) Spinner Max_Spinner;
    @BindView(R.id.btn_done) Button btn_done;
    @BindView(R.id.btn_cancel) Button btn_cancel;

    private int sp_max = 0;

    public Dialog_Max_Charge(@NonNull Context context) {
        super(context);
        this.context = context;
    }

    public Dialog getDialog() {
        return dialog;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_max_charge);
        ButterKnife.bind(this);

        dialog = this;

        getDialog().setCanceledOnTouchOutside(true);
        setCancelable(true);

        Max_list_adapter(Max_Spinner, 0);

    }

    private void Max_list_adapter(Spinner Max_Spinner, int item){
        final String[] Welfare_list = context.getResources().getStringArray(R.array.Max);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, Welfare_list);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Max_Spinner.setAdapter(adapter);
        Max_Spinner.setSelection(item);

        Max_Spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sp_max = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private int Max_value(){
        int result = 10000;
        if (sp_max==0){
            result=10000;
        }else if (sp_max<=9 && sp_max>0){
            result=(sp_max+1)*10000;
        }else if (sp_max==10){
            result=(sp_max+5)*10000;
        }else if (sp_max==11){
            result=(sp_max+9)*10000;
        }else if (sp_max==12){
            result=(sp_max+13)*10000;
        }else if (sp_max==13){
            result=(sp_max+17)*10000;
        }
        return result;
    }

    @OnClick(R.id.btn_cancel)
    void setBtnCancel() {
        getDialog().dismiss();
    }

    @OnClick(R.id.btn_done)
    void setBtnDone() {
        settings = context.getSharedPreferences("settings", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();

        editor.putString("mMax_charge", String.valueOf(Max_value()));
        editor.apply();

        getDialog().dismiss();
    }
}
