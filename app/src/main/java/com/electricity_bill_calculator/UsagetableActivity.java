package com.electricity_bill_calculator;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.electricity_bill_calculator.DTO.MyElectricity;
import com.electricity_bill_calculator.Utils.Electricity;
import com.tsengvn.typekit.TypekitContextWrapper;

import java.util.ArrayList;
import java.util.List;

import static com.electricity_bill_calculator.AnalysisActivity.ELECT;
import static com.electricity_bill_calculator.AnalysisActivity.Requisition_amount;

public class UsagetableActivity extends AppCompatActivity {

    private ListView myelectlist_view;
    private ImageView myelectlist_close;
    private TextView txt_change;
    private Spinner term_sp;
    private ImageView explan_button;

    public static List<com.electricity_bill_calculator.DTO.MyElectricity> MyElectricity;
    private ArrayList<String> Term = new ArrayList<>();
    public static MyElectricityListAdapter adapter;
    private com.electricity_bill_calculator.Utils.Electricity Electricity;
    private SharedPreferences settings;
    private View getView;

    private LayoutInflater inflater;
    private View HelpDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usagetable);
        getView = this.getWindow().getDecorView() ;

        Electricity = new Electricity();

        Initialize();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(TypekitContextWrapper.wrap(newBase));
    }

    private void Initialize (){
        myelectlist_view = (ListView) findViewById(R.id.myelectlist_view);
        myelectlist_close = (ImageView) findViewById(R.id.myelectlist_close);
        myelectlist_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        txt_change = (TextView) findViewById(R.id.txt_change);
        String str = String.format("%,d", Math.round(Requisition_amount));
        txt_change.setText("예상 전기요금 : " + str + "원");
        txt_change.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ResetDialog();
                return true;
            }
        });

        setWindow();

        settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);
        ELECT = settings.getString("ELECT", "");

        Log.d("ELECT", ELECT);

        MyElectricity = new ArrayList<MyElectricity>();
        MyElectricity.clear();

        adapter = new MyElectricityListAdapter(this.getApplicationContext(), R.layout.elect_list, MyElectricity);

        Electricity.getUsageElect(getView, ELECT, MyElectricity, adapter);
        //Collections.reverse(MyElectricity);
        myelectlist_view.setAdapter(adapter);

        setTermSpinner();

        explan_button = (ImageView) findViewById(R.id.explan_button);
        explan_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_Explanation();
            }
        });

    }

    private void setWindow(){
        // 화면 사이즈 조정
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * 0.9), (int) (height * 0.85));
    }

    private void ResetDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("알림")
                .setMessage("사용 기록을 초기화 하시겠습니까?\n(예상전기요금은 초기화되지않습니다.)")
                .setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        settings = getSharedPreferences("settings", Activity.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("ELECT", "");
                        editor.apply();

                        ELECT = settings.getString("ELECT", "");
                        finish();
                    }
                })
                .setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void setTermSpinner(){
        term_sp = (Spinner) findViewById(R.id.term_sp);

        Term.add("전체");
        Term.add(Electricity.getMonthString() + "월");
        Term.add(String.valueOf(Electricity.getBeforeMonthString(1) + "월"));
        Term.add(String.valueOf(Electricity.getBeforeMonthString(2) + "월"));

        ArrayAdapter<String> term = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Term);
        term_sp.setAdapter(term);

        term_sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0:
                        MyElectricity.clear();
                        Electricity.getUsageElect(getView, ELECT, MyElectricity, adapter);
                        break;
                    case 1:
                        MyElectricity.clear();
                        Electricity.getUsageElect(getView, ELECT, MyElectricity, adapter, Electricity.getMonthString());
                        break;
                    case 2:
                        MyElectricity.clear();
                        Electricity.getUsageElect(getView, ELECT, MyElectricity, adapter, Electricity.getBeforeMonthString(1));
                        break;
                    case 3:
                        MyElectricity.clear();
                        Electricity.getUsageElect(getView, ELECT, MyElectricity, adapter, Electricity.getBeforeMonthString(2));
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void dialog_Explanation(){
        inflater = getLayoutInflater();
        HelpDialog = inflater.inflate(R.layout.dialog_explanation, null);

        AlertDialog.Builder buider= new AlertDialog.Builder(this); //AlertDialog.Builder 객체 생성
        buider.setView(HelpDialog);

        Button btn_done = (Button) HelpDialog.findViewById(R.id.btn_done);
        btn_done.setTypeface(Typeface.createFromAsset(getAssets(), "NanumBarunGothic.otf"));

        TextView txt_help1 = (TextView) HelpDialog.findViewById(R.id.txt_help1);
        txt_help1.setTypeface(Typeface.createFromAsset(getAssets(), "NanumBarunGothic.otf"));

        TextView txt_help2 = (TextView) HelpDialog.findViewById(R.id.txt_help2);
        txt_help2.setTypeface(Typeface.createFromAsset(getAssets(), "NanumBarunGothic.otf"));


        final AlertDialog dialog = buider.create();
        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.setCanceledOnTouchOutside(true);
        dialog.show();
    }

}

