package com.electricity_bill_calculator.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.SparseArray;

import com.electricity_bill_calculator.Fragment.Fragment_Tab1;
import com.electricity_bill_calculator.Fragment.Fragment_Tab2;
import com.electricity_bill_calculator.Fragment.Fragment_Tab3;
import com.electricity_bill_calculator.Fragment.Fragment_Tab4;
import com.electricity_bill_calculator.Fragment.Fragment_Tab5;

/**
 * Created by kimjimin on 2018. 7. 29..
 */

public class Adapter_Home_ViewPager extends FragmentPagerAdapter {

    private final SparseArray<Fragment> mFragments;


    public Adapter_Home_ViewPager(FragmentManager fm) {
        super(fm);

        mFragments = new SparseArray<>();

        mFragments.put(0, new Fragment_Tab1().newInstance("tab01"));
        mFragments.put(1, new Fragment_Tab5().newInstance("tab05"));
        mFragments.put(2, new Fragment_Tab2().newInstance("tab02"));
//        mFragments.put(3, new Fragment_Tab3().newInstance("tab03"));
        mFragments.put(3, new Fragment_Tab4().newInstance("tab04"));
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }
}