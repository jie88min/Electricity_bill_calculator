package com.electricity_bill_calculator.Adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.electricity_bill_calculator.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Kimminsoo on 2016. 10. 17..
 */

public class Adapter_Tips_Recycler extends RecyclerView.Adapter<Adapter_Tips_Recycler.ViewHolder> {

    private ArrayList<String> list_item;
    private int itemLayout;
    private Context mContext = null;

    /**
     * 생성자
     *
     * @param items
     * @param itemLayout
     */
    public Adapter_Tips_Recycler(Context c, ArrayList<String> items, int itemLayout) {
        this.mContext = c;
        this.list_item = items;
        this.itemLayout = itemLayout;
    }

    /**
     * 레이아웃을 만들어서 Holer에 저장
     *
     * @param viewGroup
     * @param viewType
     * @return
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(itemLayout, viewGroup, false);
        return new ViewHolder(view);
    }

    /**
     * listView getView 를 대체
     * 넘겨 받은 데이터를 화면에 출력하는 역할
     *
     * @param viewHolder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {


        String item = list_item.get(position);

        viewHolder.tvTip.setText(item);

        viewHolder.itemView.setTag(item);
    }

    @Override
    public int getItemCount() {
        return list_item.size();
    }

    /**
     * 뷰 재활용을 위한 viewHolder
     */
    public class ViewHolder extends RecyclerView.ViewHolder {

        //로그인 정보
        public SharedPreferences prefs;
        public SharedPreferences.Editor editor;

        @BindView(R.id.tvTip) TextView tvTip;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            // todo [Init] 초기값 설정
            // 로그인 정보
            prefs = mContext.getSharedPreferences("info", MODE_PRIVATE);
            editor = prefs.edit();

        }
    }

}
