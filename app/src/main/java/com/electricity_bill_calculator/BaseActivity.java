package com.electricity_bill_calculator;

import android.app.Application;

import com.tsengvn.typekit.Typekit;

/**
 * Created by 김지민 on 2018-01-02.
 */

public class BaseActivity extends Application {
    @Override
    public void onCreate() {
        super.onCreate();

        Typekit.getInstance()
                .addNormal(Typekit.createFromAsset(this, "NanumBarunGothic.otf"))
                .addBold(Typekit.createFromAsset(this, "NanumBarunGothicBold.otf"));
    }
}
